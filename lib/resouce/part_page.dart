import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:toiecapp/constants/constant.dart';
import 'package:toiecapp/constants/fakeData.dart';
import 'package:toiecapp/model/parts.dart';
import 'package:toiecapp/resouce/home_page.dart';
import 'package:toiecapp/resouce/part_item.dart';
import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:toiecapp/resouce/profile_page.dart';
import 'package:toiecapp/resouce/test_page.dart';
import 'package:toiecapp/service/part_service.dart';
import 'package:http/http.dart' as http;


class PartPage extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FutureBuilder<List<Part>>(
        future: PartService().fetchParts(http.Client()),
        builder: (context, snapshot) {
          if (snapshot.hasError) print(snapshot.error);
          return snapshot.hasData
              ? PartListPage(listPart: snapshot.data)
              : Center(child: CircularProgressIndicator());
        },
      ),
    );
  }
}

class PartListPage extends StatelessWidget{

   List<Part> listPart;

  PartListPage({Key key, this.listPart}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: new BoxDecoration(
        gradient:  LinearGradient(
          begin: Alignment.topRight,
          end: Alignment(0, 0),
          tileMode: TileMode.repeated, 
          colors: [
            listColor.lightOrange.withOpacity(0.6),
            listColor.barColor.withOpacity(0.6),
          ],
        ),
      ),
      child: GridView(
        padding: EdgeInsets.fromLTRB(12, 50, 12, 12),
        children: listPart.map((each) => PartItems(part: each,color: listColor.barColor,)).toList(),
        gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
            maxCrossAxisExtent: 300,
            childAspectRatio: 3/2, //width / height
            crossAxisSpacing: 12,
            mainAxisSpacing: 12
        ),
      ),
    );

  }

}
