import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:toiecapp/constants/constant.dart';
import 'package:toiecapp/model/parts.dart';
import 'package:toiecapp/resouce/test_page.dart';

class PartItems extends StatelessWidget{

  Part part;
  Color color;
  PartItems({this.part, this.color});

  @override
  Widget build(BuildContext context) {
    int coutpart=this.part.partID;
    Color _color=this.color;

    return InkWell(
      onTap: (){
        Navigator.of(context).push(
            MaterialPageRoute(
                builder: (context) => TestPage(partId :part.partID)//you can send parameters using constructor
            ));
      },
      splashColor: Colors.yellow,
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(this.part.partName,
                style: TextStyle(
                  fontSize: 28,
                  fontWeight: FontWeight.bold,
                  color: listColor.mainTextColor,
                  fontFamily: 'RobotoMono',
                )),
          ],
        ),
        decoration: BoxDecoration(
            boxShadow: [
              new BoxShadow(
                color: Colors.grey[400],
                blurRadius: 4,
                spreadRadius: 0.2
              )
            ],
            color:_color,
            borderRadius: BorderRadius.circular(20)
        ),
      ),
    );
  }
}