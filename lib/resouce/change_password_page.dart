import 'package:flutter/material.dart';
import 'package:toiecapp/constants/constant.dart';
import 'package:toiecapp/model/validator.dart';
import 'package:toiecapp/service/profile_service.dart';
import 'package:http/http.dart' as http;

class ChangePassword extends StatefulWidget {

  @override
  _ChaggePasswordPage createState() => _ChaggePasswordPage();

}

class _ChaggePasswordPage extends State<ChangePassword> {

  TextEditingController _oldpassController = new TextEditingController();
  TextEditingController _passController = new TextEditingController();
  TextEditingController _comfirmpassController = new TextEditingController();
  bool isValidOldpassword=false;
  bool isValidPassworf=false;
  bool isValidConfimPassworf=false;

//  _changepasswordClick() {
//    setState(() {
//      if(validator().loginPassword(_passController.text)!=null){
//        isValidPassworf=true;
//      }else {
//        isValidPassworf=false;
//      }
//      if(validator().loginPassword(_passController.text)!=null){
//        isValidOldpassword=true;
//      }else {
//        isValidOldpassword=false;
//      }
//      if(validator().loginPassword(_passController.text)!=null){
//        isValidOldpassword=true;
//      }else {
//        isValidOldpassword=false;
//      }
//      if(!isValidPassworf){
////        AccountService().singUp(http.Client(), _nameController.text, _fullnameController.text, _emailController.text, _passController.text).then((value) => notice=value);
////        print(notice);
////        if(notice=="sign-up account successfully"){
////          Navigator.of(context).push(MaterialPageRoute(builder: (context)=> LoginPage()));
////        }
//      }
//    });
//  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: listColor.barColor,
        title: Text(
          "Change password",
          style: TextStyle(
              color: listColor.activeButton
          ),
        ),
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          color: listColor.activeButton,
          onPressed: (){
            Navigator.of(context).pop();
          },),
      ),
      body: Container(
        padding: EdgeInsets.fromLTRB(30, 0, 30, 0),
        constraints: BoxConstraints.expand(),
        color: Colors.white,
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              SizedBox(
                height: 60,
              ),
              /*Image.asset('images/lock_im.jpg'),*/
              Icon(Icons.lock_outline,size: 200,color: listColor.activeButton,),
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 40, 0, 20),
                child: StreamBuilder(
                  //stream: authBloc.nameStream,
                    builder: (context, snapshot) =>
                        TextField(
                          controller: _oldpassController,
                          obscureText: true,
                          style: TextStyle(fontSize: 18, color: Colors.black),
                          decoration: InputDecoration(
                              errorText:
                              snapshot.hasError ? snapshot.error : null,
                              labelText: "Current PassWord ",
                              prefixIcon: Container(
                                  width: 50, child: Image.asset("images/ic_lock.png")),
                              border: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Color(0xffCED0D2), width: 1),
                                  borderRadius:
                                  BorderRadius.all(Radius.circular(6)))),
                        )),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 20, 0, 20),
                child: StreamBuilder(
                    builder: (context, snapshot) =>
                        TextField(
                          controller: _passController,
                          obscureText: true,
                          style: TextStyle(fontSize: 18, color: Colors.black),
                          decoration: InputDecoration(
                              labelText: "New Password",
                              errorText:
                              snapshot.hasError ? snapshot.error : null,
                              prefixIcon: Container(
                                  width: 50, child: Image.asset("images/ic_lock.png")),
                              border: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Color(0xffCED0D2), width: 1),
                                  borderRadius:
                                  BorderRadius.all(Radius.circular(6)))),
                        )),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 20, 0, 20),
                child: StreamBuilder(
                  // stream: authBloc.emailStream,
                    builder: (context, snapshot) =>
                        TextField(
                          controller: _comfirmpassController,
                          obscureText: true,
                          style: TextStyle(fontSize: 18, color: Colors.black),
                          decoration: InputDecoration(
                              labelText: "Confirm Password",
                              errorText:
                              snapshot.hasError ? snapshot.error : null,
                              prefixIcon: Container(
                                  width: 50, child: Image.asset("images/ic_lock.png")),
                              border: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Color(0xffCED0D2), width: 1),
                                  borderRadius:
                                  BorderRadius.all(Radius.circular(6)))),
                        )),
              ),
              Padding(
                  padding: const EdgeInsets.fromLTRB(0, 20, 0, 10),
                  child: Container(
                    //margin: EdgeInsets.fromLTRB(40,0, 0, 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      mainAxisSize: MainAxisSize.max,
                      children: <Widget>[
                        ButtonTheme(
                          height: 30,
                          child: RaisedButton(
                            // onPressed: _onSignUpClicked,
                            child: Text(
                              "Update",
                              style: TextStyle(color: listColor.barColor, fontSize: 18),
                            ),
                            onPressed: () async{
                              Map<String, dynamic> params = Map<String, dynamic>();
                              params["oldPassword"] = _oldpassController.text;
                              params["newPassword"] = _passController.text;
                              params["confirmPassword"] = _comfirmpassController.text;
                              await ProfileService().updatePassword(http.Client(), params);
                              Navigator.of(context).pop();
                            },
                            color: listColor.activeButton,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.all(Radius.circular(6))),
                          ),
                        ),
                        Padding(padding: const EdgeInsets.fromLTRB(20, 0,0, 10)),
                        ButtonTheme(
                          height: 30,
                          child: RaisedButton(
                            // onPressed: _onSignUpClicked,
                            child: Text(
                              "Cancel",
                              style: TextStyle(color: Colors.white, fontSize: 18),
                            ),
                            onPressed: (){
                              Navigator.of(context).pop();
                            },
                            color: listColor.activeButton,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.all(Radius.circular(6))),
                          ),
                        ),
                      ],
                    ),
                  )
              ),
            ],
          ),
        ),
      ),
    );
  }
}