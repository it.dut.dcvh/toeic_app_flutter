import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:toiecapp/config/app_config.dart';
import 'package:toiecapp/constants/constant.dart';
import 'package:toiecapp/model/group_question.dart';

class GroupQuestionItem extends StatelessWidget{
  GroupQuestion groupQuestion;

  GroupQuestionItem(this.groupQuestion);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView(
        children: [
          Container(
            margin: const EdgeInsets.all(10),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10.0),
            ),
            child: Column(
              children: [
                _buildAudio(groupQuestion),
                _buildText(groupQuestion),
                _buildImage(groupQuestion),
              ],
            )
          ),
          _buildListQuestion(groupQuestion.questions),
          Container(
            height: 60,
            width: MediaQuery.of(context).size.width,
          )
        ],
      )
    );
  }

  Widget _buildImage(groupQuestion) {
    return Builder(builder: (context) {
      if (groupQuestion.imagePath is String &&
          groupQuestion.imagePath.toString() != 'undefined' && 
          groupQuestion.imagePath.toString() != '') {
        return Container(
          width: MediaQuery.of(context).size.width,
          child: Image.network(
            pod_urlconst.URL_PUBLIC_IMAGE+groupQuestion.imagePath,
            fit: BoxFit.cover,
          ),
        );
      }
      return Container(
        width: MediaQuery.of(context).size.width,
      );
    });
  }

  Widget _buildText(groupQuestion) {
    return Builder(builder: (context) {
      if (groupQuestion.text is String && groupQuestion.text != '') {
        return Container(
          padding: const EdgeInsets.fromLTRB(10, 10, 10, 20),
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(2.0),
            color: listColor.barColor,
            boxShadow: [
              new BoxShadow(
                color: Colors.grey[600],
                offset: new Offset(0.0, 5.0),
                blurRadius: 10,
              )
            ],
          ),
          child: Text(
            groupQuestion.text,
            style: TextStyle(
              fontSize: 15,
              color: listColor.outLineButtonTextColor
            ),
            textAlign: TextAlign.left,
            textWidthBasis: TextWidthBasis.parent,
            
          ),
        );
      }
        return Container(
          width: MediaQuery.of(context).size.width,
          height: 20,
        );
    });
  }

  Widget _buildAudio(groupQuestion) {
    return Builder(builder: (context) {
      if (groupQuestion.audioPath is String &&
          groupQuestion.audioPath.toString() != 'undefined' && 
          groupQuestion.audioPath.toString() != '') {
        return Container(
          height: 70,
        );
      }

      return Container(
        width: MediaQuery.of(context).size.width,
      );
    });
  }

  Widget _buildListQuestion (questions) {
    return Builder(builder: (context) {
      print(questions);
      return Container(
        width: MediaQuery.of(context).size.width,
        child: Column(
          children: _buildQuestion(questions),
        )
      );
    });
  }

  List<Widget> _buildQuestion (questions) {
    return new List<Widget>.generate(
      questions.length,
      (index) {
        return Builder(builder: (context) {
          return Container(
            width: MediaQuery.of(context).size.width,
            margin: const EdgeInsets.fromLTRB(20, 10, 20, 10),
            decoration: BoxDecoration(
              border: Border(
                bottom: BorderSide(
                  color: listColor.outLineButtonTextColor,
                  width: 1.0,
                )
              ),
            ),
            child: Column(
              children: [
                Container(
                  padding: const EdgeInsets.fromLTRB(5, 5, 5, 10),
                  width: MediaQuery.of(context).size.width,
                  child: Text(
                    questions[index].text,
                    textAlign: TextAlign.left,
                    textWidthBasis: TextWidthBasis.parent,
                  ),
                ),
                Column(
                  children: _buildListAnswers(questions[index].answers),
                )
                
              ],
            ),
          );
        });
      }
    );
  }

  List<Widget> _buildListAnswers (answers) {
    return List<Widget>.generate(
      answers.length, 
      (index) {
        return Container(
          child: Row(
            children: [
              Radio(
                value: index,
                groupValue: null,
                onChanged: (index) {
                  
                }
              ),
              Text(
                answers[index].text,
              )
            ],
          )
        );
      }
    );
  }
}
