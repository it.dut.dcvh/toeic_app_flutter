import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:toiecapp/constants/constant.dart';
import 'package:toiecapp/resouce/history_page.dart';
import 'package:toiecapp/resouce/home_page.dart';

class IndexPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          color: listColor.grayBackGround
        ),
        child: ListView(
          children: [
            Container(
              height: 260,
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.fromLTRB(0, 0, 0, 10),
              margin: EdgeInsets.fromLTRB(0, 0, 0, 30),
              color: listColor.barColor,
              child: Stack(
                children: [
                  Align(
                    alignment: Alignment.topCenter,
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      height: 175,
                      decoration: BoxDecoration(
                        image:  DecorationImage(
                          image: AssetImage('images/bannerr.png'),
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ),
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: Container(
                      width: 150,
                      height: 150,
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          image: AssetImage('images/logoo.png'),
                          fit: BoxFit.cover,
                        ),
                        shape: BoxShape.circle,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: 60,
              padding: EdgeInsets.all(10),
              child:  Text(
                "Practice",
                textAlign: TextAlign.left,
                style: TextStyle(
                  fontSize: 25,
                  fontWeight: FontWeight.bold,
                  color: listColor.activeButton
                ),
              )
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: 120,
              padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
              child:  ListView(
                scrollDirection: Axis.horizontal,
                children: [
                  InkWell(
                    child: Container(
                      margin: EdgeInsets.fromLTRB(0, 0, 10, 0),
                      padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
                      width: 100,
                      height: 100,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8.0),
                          color: Colors.white,
                          boxShadow: [
                            BoxShadow(
                                color: Colors.grey[400],
                                blurRadius: 6,
                                spreadRadius: 0.2
                            )
                          ]
                      ),
                      child: Column(
                        children: <Widget>[
                          Icon(Icons.rate_review,size: 60,color: listColor.activeButton,),
                          Text("Practice")
                        ],
                      ),
                    ),
                    onTap: (){
                      Navigator.of(context).push(MaterialPageRoute(builder: (context) => HomePage(pageIndex: 1,)));
                    },
                  ),
                 InkWell(
                   child:  Container(
                     margin: EdgeInsets.fromLTRB(0, 0, 10, 0),
                     padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
                     width: 100,
                     height: 100,
                     decoration: BoxDecoration(
                         borderRadius: BorderRadius.circular(8.0),
                         color: Colors.white,
                         boxShadow: [
                           BoxShadow(
                               color: Colors.grey[400],
                               blurRadius: 6,
                               spreadRadius: 0.2
                           )
                         ]
                     ),
                     child: Column(
                       children: <Widget>[
                         Icon(Icons.event_available,size: 60,color: listColor.activeButton,),
                         Text("Rank")
                       ],
                     ),
                   ),
                   onTap: (){
                     Navigator.of(context).push(MaterialPageRoute(builder: (context) => HomePage(pageIndex: 3,)));
                   },
                 ),
                ],
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: 60,
              padding: EdgeInsets.all(10),
              child:  Text(
                "Personal",
                textAlign: TextAlign.left,
                style: TextStyle(
                  fontSize: 25,
                  fontWeight: FontWeight.bold,
                  color: listColor.activeButton
                ),
              )
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: 120,
              padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
              child:  ListView(
                scrollDirection: Axis.horizontal,
                children: [
                  InkWell(
                    child: Container(
                      margin: EdgeInsets.fromLTRB(0, 0, 10, 0),
                      padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
                      width: 100,
                      height: 100,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8.0),
                          color: Colors.white,
                          boxShadow: [
                            BoxShadow(
                                color: Colors.grey[400],
                                blurRadius: 6,
                                spreadRadius: 0.2
                            )
                          ]
                      ),
                      child: Column(
                        children: <Widget>[
                          Icon(Icons.person,size: 60,color: listColor.activeButton,),
                          Text("Profile")
                        ],
                      ),
                    ),
                    onTap: (){
                      Navigator.of(context).push(MaterialPageRoute(builder: (context) => HomePage(pageIndex: 2,)));
                    },
                  ),
                  InkWell(
                    child: Container(
                      margin: EdgeInsets.fromLTRB(0, 0, 10, 0),
                      padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
                      width: 100,
                      height: 100,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8.0),
                          color: Colors.white,
                          boxShadow: [
                            BoxShadow(
                                color: Colors.grey[400],
                                blurRadius: 6,
                                spreadRadius: 0.2
                            )
                          ]
                      ),
                      child: Column(
                        children: <Widget>[
                          Icon(Icons.history,size: 60,color: listColor.activeButton,),
                          Text("History")
                        ],
                      ),
                    ),
                    onTap: (){
                      Navigator.of(context).push(MaterialPageRoute(builder: (context) => HistoryPage()));
                    },
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}