import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:toiecapp/config/app_config.dart';
import 'package:toiecapp/constants/constant.dart';
import 'package:toiecapp/constants/fakeData.dart';
import 'package:toiecapp/resouce/group_question_item.dart';
import 'package:toiecapp/model/group_question.dart';
import 'package:toiecapp/resouce/home_page.dart';
import 'package:toiecapp/service/group_question_service.dart';
import 'package:http/http.dart' as http;

class Record {
  int answerId;
  bool isChoose;
  bool isCorrect;
  int questionId;
  String flag = '';
  Record(this.answerId, this.isChoose, this.isCorrect, this.questionId) {
    this.flag = '$answerId-$questionId';
  }
}

class GroupValue {
  int questionId;
  String flag = '';
  GroupValue(this.questionId);
}

class ChooseAnswer {
  int questionId;
  int answerId;
  bool isCorrect;

  ChooseAnswer(this.questionId, this.answerId, this.isCorrect);
}

class ResultPage extends StatefulWidget {

  final int testId;
  List<ChooseAnswer> listChooseAnswer;
  List<GroupQuestion> listGroupQuestion;
  int totalCorrectAnswers ;

  ResultPage({this.testId, this.listChooseAnswer, this.listGroupQuestion, this.totalCorrectAnswers});

  @override
  State<StatefulWidget> createState () {
    return _ResultPage(testId, listGroupQuestion, listChooseAnswer, totalCorrectAnswers);
  }

}

class _ResultPage extends State<ResultPage> {
  int current;
  GroupQuestion currentGroupQuestion;
  List<GroupQuestion> listGroupQuestions;
  List<ChooseAnswer> listChooseAnswers = new List<ChooseAnswer>();
  int testId;
  int totalQuestionsCorrect;
  int totalQuestions = 0;
  List<GroupValue> groupValues = new List<GroupValue>();

  AudioPlayer audioPlayer = AudioPlayer();
  double maxVol = 100;
  double volume = 50;
  List<IconData> speakerIcons = [Icons.volume_off, Icons.volume_mute, Icons.volume_down, Icons.volume_up];
  int indexIconVolume = 2;

  List<Color> listColorAnswer = [Colors.red.withOpacity(0.8), Colors.green.withOpacity(0.8)];

  _ResultPage(this.testId, this.listGroupQuestions, this.listChooseAnswers, this.totalQuestionsCorrect);

  @override
  void initState() {
     super.initState();
     maxVol;
     volume;
     indexIconVolume;
     current = 0;
     totalQuestions = getTotalQuestion();
     currentGroupQuestion = listGroupQuestions[current];
     this.audioPlayer.dispose();
     String playUrl = pod_urlconst.URL_PUBLIC_AUDIO+currentGroupQuestion.audioPath;
     this.audioPlayer.play(playUrl, isLocal: false);
  }

  @override
  Widget build(BuildContext context) {
    return new WillPopScope(
        child: Scaffold(
            appBar: AppBar(
              backgroundColor: listColor.activeButton,
              leading: IconButton(
                  icon: new Icon(Icons.arrow_back_ios, color: listColor.barColor),
                  onPressed: () {
                    setState(() {
                      this.audioPlayer.dispose();
                      this.audioPlayer.stop();
                    });
                    Navigator.of(context).pop();
                  }
              ),
            ),
            body: Stack(
              children: [
                Container(
                    width: MediaQuery.of(context).size.width,
                    child: Builder(builder: (context) {
                      return _buildGroupQuestionItem(this.currentGroupQuestion) ;
                    })
                ),
                Align(
                    alignment: Alignment.bottomCenter,
                    child: Container(
                      margin: const EdgeInsets.all(6.0),
                      height: 60,
                      width: MediaQuery.of(context).size.width,
                      decoration: new BoxDecoration(
                        color: listColor.activeButton.withOpacity(0.8),
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child:  Stack(
                        fit: StackFit.expand,
                        children: [
                          Align(
                              alignment: Alignment.centerLeft,
                              child: Container(
                                width: 40,
                                child: IconButton(
                                  icon: Icon(Icons.arrow_back_ios),
                                  color: Colors.white,

                                  onPressed: () {
                                    _getPrevQuestion();
                                  },
                                ),
                              )
                          ),
                          Align(
                              alignment: Alignment.centerRight,
                              child: Container(
                                width: 40,
                                child: IconButton(
                                  icon: Icon(Icons.arrow_forward_ios),
                                  color: Colors.white,

                                  onPressed: () {
                                    _getnextQuestion();
                                  },
                                ),
                              )
                          )

                        ],
                      ),
                    )
                ),
              ],
            )
        ),
        onWillPop: (){
          setState(() {
            this.audioPlayer.dispose();
            this.audioPlayer.stop();
          });
          Navigator.of(context).pop();
        },
    );
  }

  Widget _buildGroupQuestionItem(groupQuestions) {
    return Container(
        child: ListView(
          children: [
            Container(
                margin: const EdgeInsets.all(10),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10.0),
                ),
                child: Column(
                  children: [
                    _buildAudioPlayer(),
                    _buildText(groupQuestions),
                    _buildImage(groupQuestions),
                  ],
                )
            ),
            _buildListQuestion(groupQuestions.questions),
            Container(
              height: 60,
              width: MediaQuery.of(context).size.width,
            )
          ],
        )
    );
  }

  Widget _buildAudioPlayer() {
    return Container (
        width: MediaQuery.of(context).size.width,
        margin: const EdgeInsets.fromLTRB(10, 10, 10, 0),
        height: 60,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8.0),
            color: listColor.barColor,
            boxShadow: [(
                BoxShadow(
                    color: Colors.grey[400],
                    blurRadius: 10.0,
                    spreadRadius: 2.0
                )
            ),]
        ),
        child: Stack(children: [
          Align(
            alignment: Alignment.centerRight,
            child: Container(
              alignment: Alignment.centerRight,
              width: MediaQuery.of(context).size.width - 150,
              child: Stack(
                children: [
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Icon(speakerIcons[indexIconVolume],),
                  ),
                  Slider(
                      label: 'vol',
                      activeColor: listColor.activeButton,
                      inactiveColor: Colors.grey[300],
                      value: this.volume,
                      min: 0.0,
                      max: this.maxVol,
                      onChanged: (double value) {
                        setState(() {
                          this.volume = value;
                          if(value < 80 && value > 20) {
                            indexIconVolume = 2;
                          }

                          if(value >= 80) {
                            indexIconVolume = 3;
                          }

                          if(value <= 20 && value > 0) {
                            indexIconVolume = 1;
                          }

                          if (value == 0 ) {
                            indexIconVolume = 0;
                          }
                        });
                      }
                  )
                ],
              ),
            ),
          ),
          Align(
            alignment: Alignment.centerLeft,
            child: IconButton(
              icon: Icon(
                Icons.play_circle_outline,
                color: listColor.activeButton,
              ),
              iconSize: 40.0,
              onPressed: () {

              },
            ),
          )
        ],)
    );
  }

  Widget _buildImage(groupQuestion) {
    return Builder(builder: (context) {
      if (groupQuestion.imagePath is String &&
          groupQuestion.imagePath.toString() != 'undefined' &&
          groupQuestion.imagePath.toString() != '') {
        return Container(
          width: MediaQuery.of(context).size.width,
          child: Image.network(
            pod_urlconst.URL_PUBLIC_IMAGE+groupQuestion.imagePath,
            fit: BoxFit.cover,
          ),
        );
      }
      return Container(
        width: MediaQuery.of(context).size.width,
      );
    });
  }

  Widget _buildText(groupQuestion) {
    return Builder(builder: (context) {
      if (groupQuestion.text is String && groupQuestion.text != '') {
        return Container(
          margin: const EdgeInsets.fromLTRB(0, 20, 0, 0),
          padding: const EdgeInsets.fromLTRB(10, 10, 10, 20),
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(2.0),
            color: listColor.barColor,
            boxShadow: [
              new BoxShadow(
                color: Colors.grey[400],
                offset: new Offset(0.0, 5.0),
                blurRadius: 10,
              )
            ],
          ),
          child: Text(
            groupQuestion.text,
            style: TextStyle(
                fontSize: 15,
                color: listColor.activeButton
            ),
            textAlign: TextAlign.left,
            textWidthBasis: TextWidthBasis.parent,

          ),
        );
      }
      return Container(
        width: MediaQuery.of(context).size.width,
        height: 20,
      );
    });
  }

  Widget _buildListQuestion(questions) {
    return Builder(builder: (context) {
      return Container(
          width: MediaQuery.of(context).size.width,
          child: Column(
            children: _buildQuestion(questions),
          )
      );
    });
  }

  List<Widget> _buildQuestion(questions) {
    return new List<Widget>.generate(
        questions.length,
            (index) {
          return Builder(builder: (context) {
            return Container(
              width: MediaQuery.of(context).size.width,
              margin: const EdgeInsets.fromLTRB(20, 10, 20, 10),
              decoration: BoxDecoration(
                border: Border(
                    bottom: BorderSide(
                      color: listColor.activeButton,
                      width: 1.0,
                    )
                ),
              ),
              child: Column(
                children: [
                  Container(
                    padding: const EdgeInsets.fromLTRB(5, 5, 5, 10),
                    width: MediaQuery.of(context).size.width,
                    child: Text(
                      questions[index].text,
                      textAlign: TextAlign.left,
                      textWidthBasis: TextWidthBasis.parent,
                      style: TextStyle(
                        fontSize: 20,
                      ),
                    ),
                  ),
                  Column(
                    children: _buildListAnswers(questions[index].answers, questions[index].questionId),
                  ),
                  Container(
                    child: Text('Explanation',
                      textAlign: TextAlign.left,
                      textWidthBasis: TextWidthBasis.parent,
                      style: TextStyle(
                        fontSize: 25.0,
                      ),
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    padding: EdgeInsets.all(10),
                    margin: EdgeInsets.fromLTRB(10, 5, 10, 5),
                    child: Text(
                      questions[index].explanation,
                      textAlign: TextAlign.left,
                      textWidthBasis: TextWidthBasis.parent,
                      style: TextStyle(
                        fontSize: 20,
                      ),
                    )
                  )


                ],
              ),
            );
          });
        }
    );
  }

  List<Widget> _buildListAnswers (answers, questionId) {
    // int choose;
    return List<Widget>.generate(
        answers.length,
            (index) {
          int groupValue = checkIsChoose(answers[index].answerId)? answers[index].answerId : -1000;
          Color check = answers[index].isRight ? this.listColorAnswer[1] : this.listColorAnswer[0];
          String text = answers[index].text != null ? answers[index].text : '';
          return Container(
            
            color: check,
            child: RadioListTile(
              title: Text(text),
              value: answers[index].answerId,
              groupValue: groupValue,
              onChanged: ( count) {

              },
            ),
          );
        }
    );
  }

  _getnextQuestion () {
    setState(() {
      if (current < listGroupQuestions.length -1) {
        current++;
        currentGroupQuestion = listGroupQuestions[current];
        // print(currentGroupQuestion.imagePath);
        print(current);
         this.audioPlayer.dispose();
         String playUrl = pod_urlconst.URL_PUBLIC_AUDIO+currentGroupQuestion.audioPath;
         this.audioPlayer.play(playUrl, isLocal: false);
      }

    });
  }

  _getPrevQuestion () {
    setState(() {
      if (current > 0) {
        current--;
        currentGroupQuestion = listGroupQuestions[current];
        // print(currentGroupQuestion.audioPath);
        print(current);
         this.audioPlayer.dispose();
         String playUrl = pod_urlconst.URL_PUBLIC_AUDIO+currentGroupQuestion.audioPath;
         this.audioPlayer.play(playUrl, isLocal: false);
      }
    });
  }

  int getTotalQuestion () {
    int total = 0;
    this.listGroupQuestions.forEach( (groupQuestion) {
      total += groupQuestion.questions.length;
    });

    return total;
  }

  bool checkIsChoose(answerId) {
    bool isChoose = false;
    this.listChooseAnswers.forEach((element) {
      if (element.answerId == answerId) {
        isChoose = true;
      }
    });

    return isChoose;
  }
}
