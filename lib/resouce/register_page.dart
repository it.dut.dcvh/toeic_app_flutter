import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:toiecapp/constants/constant.dart';
import 'package:toiecapp/model/validator.dart';
import 'package:toiecapp/resouce/login_page.dart';
import 'package:toiecapp/service/account_service.dart';
import 'package:http/http.dart' as http;

class RegisterPage extends StatefulWidget {
  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {

  TextEditingController _nameController = new TextEditingController();
  TextEditingController _emailController = new TextEditingController();
  TextEditingController _passController = new TextEditingController();
  TextEditingController _fullnameController = new TextEditingController();
  TextEditingController _confimController = new TextEditingController();


  bool isValidEmail =false;
  bool isValidPassword=false;
  bool isValidUsername=false;
  bool isValidFullname=false;
  bool isValidConfimPassword=false;

  Notice notice;

  void showTopShortToastSucces(String notice) {
    Fluttertoast.showToast(
        msg: notice,
        textColor: Colors.black,
        backgroundColor: listColor.redAccent,
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.TOP,
        timeInSecForIosWeb: 2,
        fontSize:20
    );
  }

  _registerClick() {
    print('1232');
    setState(() {
      if(validator().validateEmail(_emailController.text)!=null){
        isValidEmail=true;
      }else {
        isValidEmail=false;
      }
      if(validator().emptyUsername(_nameController.text)!=null){
        isValidUsername=true;
      }else {
        isValidUsername=false;
      }
      if(validator().emptyfullname(_fullnameController.text)!=null){
        isValidFullname=true;
      }else {
        isValidFullname=false;
      }
      if(validator().loginPassword(_passController.text)!=null){
        isValidPassword=true;
      }else {
        isValidPassword=false;
      }
      if(validator().validateConfimPassWord(_confimController.text,_passController.text)!=null){
        isValidConfimPassword=true;
      }else {
        isValidConfimPassword=false;
      }
      if(!isValidEmail && !isValidPassword && !isValidUsername && !isValidFullname && !isValidConfimPassword){
        AccountService().singUp(http.Client(), _nameController.text, _fullnameController.text, _emailController.text, _passController.text).then((value) {
          if(value.isSuccess){

            Navigator.of(context).push(MaterialPageRoute(builder: (context)=> LoginPage()));
          }else{
            showTopShortToastSucces(value.message);
          }
        });
      }
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.fromLTRB(30, 0, 30, 0),
        constraints: BoxConstraints.expand(),
        color: Colors.white,
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              SizedBox(
                height: 100,
              ),
              Image.asset('images/toiec.jpg'),
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 50, 0, 20),
                child: StreamBuilder(
                  //stream: authBloc.nameStream,
                    builder: (context, snapshot) =>
                        TextField(
                          controller: _nameController,
                          style: TextStyle(fontSize: 18, color: Colors.black),
                          decoration: InputDecoration(
                              errorText:isValidUsername ? validator().emptyUsername(_nameController.text) : null,
                              labelText: "UserName",
                              prefixIcon: Container(
                                  width: 50, child: Image.asset("images/ic_user.png")),
                              border: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Color(0xffCED0D2), width: 1),
                                  borderRadius:
                                  BorderRadius.all(Radius.circular(6)))),
                        )),
              ),
              StreamBuilder(
                // stream: authBloc.phoneStream,
                  builder: (context, snapshot) =>
                      TextField(
                        controller: _fullnameController,
                        style: TextStyle(fontSize: 18, color: Colors.black),
                        decoration: InputDecoration(
                            labelText: "Full name",
                            errorText: isValidFullname ? validator().emptyfullname(_fullnameController.text) : null,
                            prefixIcon: Container(
                                width: 50, child: Image.asset("images/ic_user.png")),
                            border: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Color(0xffCED0D2), width: 1),
                                borderRadius:
                                BorderRadius.all(Radius.circular(6)))),
                      )),
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 20, 0, 20),
                child: StreamBuilder(
                  // stream: authBloc.emailStream,
                    builder: (context, snapshot) =>
                        TextField(
                          controller: _emailController,
                          style: TextStyle(fontSize: 18, color: Colors.black),
                          decoration: InputDecoration(
                              labelText: "Email",
                              errorText:  isValidEmail ? validator().validateEmail(_emailController.text) : null,
                              prefixIcon: Container(
                                  width: 50, child: Image.asset("images/ic_mail.png")),
                              border: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Color(0xffCED0D2), width: 1),
                                  borderRadius:
                                  BorderRadius.all(Radius.circular(6)))),
                        )),
              ),
              StreamBuilder(
                // stream: authBloc.passStream,
                  builder: (context, snapshot) =>
                      TextField(
                        controller: _passController,
                        obscureText: true,
                        style: TextStyle(fontSize: 18, color: Colors.black),
                        decoration: InputDecoration(
                            errorText:isValidPassword ? validator().loginPassword(_passController.text) : null,
                            labelText: "Password",
                            prefixIcon: Container(
                                width: 50, child: Image.asset("images/ic_lock.png")),
                            border: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Color(0xffCED0D2), width: 1),
                                borderRadius:
                                BorderRadius.all(Radius.circular(6)))),
                      )),
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 20, 0, 20),
                child: StreamBuilder(
                  // stream: authBloc.passStream,
                    builder: (context, snapshot) =>
                        TextField(
                          controller: _confimController,
                          obscureText: true,
                          style: TextStyle(fontSize: 18, color: Colors.black),
                          decoration: InputDecoration(
                              errorText:isValidConfimPassword ? validator().validateConfimPassWord(_confimController.text,_passController.text) : null,
                              labelText: "Confirm Password",
                              prefixIcon: Container(
                                  width: 50, child: Image.asset("images/ic_lock.png")),
                              border: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Color(0xffCED0D2), width: 1),
                                  borderRadius:
                                  BorderRadius.all(Radius.circular(6)))),
                        )),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 30, 0, 40),
                child: SizedBox(
                  width: double.infinity,
                  height: 52,
                  child: RaisedButton(
                    // onPressed: _onSignUpClicked,
                    child: Text(
                      "Sign up",
                      style: TextStyle(color: Colors.white, fontSize: 18),
                    ),
                    onPressed: (){
                      _registerClick();
                    },
                    color: listColor.activeButton,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(6))),
                  ),

                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 0, 0, 40),
                child: RichText(
                  text: TextSpan(
                      text: "Already a User? ",
                      style: TextStyle(color: Color(0xff606470), fontSize: 16),
                      children: <TextSpan>[
                        TextSpan(
                            recognizer: TapGestureRecognizer()
                              ..onTap = () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => LoginPage()));
                              },
                            text: "Login now",
                            style: TextStyle(
                                color: listColor.activeButton, fontSize: 16))
                      ]),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}