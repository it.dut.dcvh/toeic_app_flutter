import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:toiecapp/constants/constant.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:toiecapp/resouce/index_page.dart';
import 'package:toiecapp/resouce/login_page.dart';
import 'package:toiecapp/resouce/part_page.dart';
import 'package:toiecapp/resouce/profile_page.dart';
import 'package:toiecapp/resouce/rank_test.dart';
import 'package:toiecapp/resouce/test_page.dart';

class HomePage extends StatefulWidget{
  int pageIndex=0;
  HomePage({this.pageIndex});
  @override
  _homepage createState()=> _homepage(pageIndex: pageIndex);
}

class _homepage extends State<HomePage>{

  SharedPreferences sharedPreferences;
  int pageIndex=0;

  _homepage({this.pageIndex});
  @override
  void initState() {
    super.initState();
    checkLoginStatus();
  }

  final pageList=[
    IndexPage(),
    PartPage(),
    ProfilePage(),
    RankPage(),
  ];

  checkLoginStatus() async {
    sharedPreferences = await SharedPreferences.getInstance();
    print(sharedPreferences.getString("token"));
    if (sharedPreferences.getString("token") == null) {
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (BuildContext context) => LoginPage()), (
          Route<dynamic> route) => false);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: pageList[pageIndex],
      bottomNavigationBar: Container(
        child: BottomNavigationBar(
          currentIndex: pageIndex,
          type: BottomNavigationBarType.fixed,
          backgroundColor: listColor.barColor,
          items: [
            BottomNavigationBarItem(
                icon: Icon(
                  Icons.home,
                  color: listColor.inActiveButton,
                  size: 30,
                ),
                activeIcon: Icon(
                  Icons.home,
                  color: listColor.activeButton,
                  size: 30,
                ),
                title: Text(
                  "Home",
                  style: TextStyle(
                    color: listColor.activeButton
                  ),
                ),

            ),
            BottomNavigationBarItem(
                icon: Icon(
                  Icons.rate_review,
                  color: listColor.inActiveButton,
                  size: 30,
                ),
                activeIcon: Icon(
                  Icons.rate_review,
                  color: listColor.activeButton,
                  size: 30,
                ),
                title: Text(
                  "Pratice",
                  style: TextStyle(
                    color: listColor.activeButton
                  ),
                ),
                backgroundColor: Colors.blue
            ),
            BottomNavigationBarItem(
                icon: Icon(
                  Icons.person,
                  color: listColor.inActiveButton,
                  size: 30,
                ),
                activeIcon: Icon(
                  Icons.person,
                  color: listColor.activeButton,
                  size: 30,
                ),
                title: Text(
                  "Profile",
                  style: TextStyle(
                    color: listColor.activeButton
                  ),
                ),
                backgroundColor: Colors.blue
            ),
            BottomNavigationBarItem(
                icon: Icon(
                  Icons.event_available,
                  color: listColor.inActiveButton,
                  size: 30,
                ),
                activeIcon: Icon(
                  Icons.event_available,
                  color: listColor.activeButton,
                  size: 30,
                ),
                title: Text(
                  "rank",
                  style: TextStyle(
                    color: listColor.activeButton
                  ),
                ),
                backgroundColor: Colors.blue
            )
          ],
          onTap: (index){
            setState(() {
              pageIndex=index;
            });
          },
        ),
      )

    );
  }
}