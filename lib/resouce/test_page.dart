import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:toiecapp/constants/constant.dart';
import 'package:toiecapp/model/group_question.dart';
import 'package:toiecapp/model/tests.dart';
import 'package:toiecapp/resouce/group_question_page.dart';
import 'package:toiecapp/service/test_service.dart';
import 'package:http/http.dart' as http;

class TestPage extends StatelessWidget{

  final int partId;
  TestPage({this.partId}):super();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: AppBar(
        backgroundColor: listColor.activeButton,
        title: Text(
          "List Tests",
          style: TextStyle(
            color: listColor.barColor
          ),
        ),
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios),
          color: listColor.barColor,
          onPressed: (){
            Navigator.of(context).pop();
          },),
      ),
      body:FutureBuilder(
          future: TestService().fetchTest(http.Client(), partId),
          builder: (context, snapshot){
            if (snapshot.hasError) print(snapshot.error);
            return snapshot.hasData
                ? TestList(listTest : snapshot.data)
                : Center(child: CircularProgressIndicator());
          }),
    );
  }
}
class TestList extends StatelessWidget{

  List<Test> listTest;

  TestList({Key key,this.listTest}) : super(key: key);

  List<GroupQuestion> listGroupQuestions ;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView.separated(
        separatorBuilder: (_, __) => Divider(height: 1, color: Colors.green,),
        itemBuilder: (_, index){
          return Card(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10)
              ),
              child: Row(
                children: <Widget>[
                  Padding(padding: const EdgeInsets.fromLTRB(0, 90,0,0)),
                  Expanded(
                    child: InkWell(
                      child: Container(
                        decoration:BoxDecoration(
                            boxShadow: [
                              new BoxShadow(
                                  color: Colors.grey[400],
                                  blurRadius: 10,
                                  spreadRadius: 0.2
                              )
                            ],
                            color:listColor.barColor,
                            borderRadius: BorderRadius.circular(10)
                        ) ,
                        child: Column(
                          children: <Widget>[
                            Container(
                              child: ListTile(
                                title: Text(this.listTest[index].name, style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 18,
                                  color: listColor.activeButton,
                                ),),
                              ),
                              decoration: BoxDecoration(
                                  border: Border(
                                      bottom: BorderSide(
                                          width: 1,
                                          color: Colors.grey[200]
                                      )
                                  )
                              ),
                            ),
                            Row(
                              children: <Widget>[
                                Padding(
                                  padding: EdgeInsets.fromLTRB(10,5, 0, 5),
                                  child: Row(
                                    children: List.generate(5, (numberstar){
                                      return IconTheme(
                                        data: IconThemeData(
                                          color: Colors.amber,
                                          size: 35,
                                        ),
                                        child: Icon(
                                          numberstar < this.listTest[index].averagerate ? Icons.star : Icons.star_border,
                                        ),
                                      );
                                    }),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                      onTap: () {
                        Navigator.of(context)
                            .push(MaterialPageRoute(builder: (_) => GroupQuestionPage(testId: this.listTest[index].testId,)));
                      },
                    )
                  )
                ],
              )
          );
        },
        itemCount: this.listTest.length,
      ),
    );
  }
}

