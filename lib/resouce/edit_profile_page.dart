import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:toiecapp/constants/constant.dart';
import 'package:toiecapp/model/profile.dart';
import 'package:toiecapp/resouce/home_page.dart';
import 'package:toiecapp/resouce/profile_page.dart';
import 'package:toiecapp/service/profile_service.dart';
import 'package:http/http.dart' as http;

class EditProfilePage extends StatefulWidget{

@override
_Editprofilepage createState() => _Editprofilepage();

}

class _Editprofilepage extends State<EditProfilePage>{

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: listColor.barColor,
          title: Text(
            "Test list",
            style: TextStyle(
                color: listColor.activeButton
            ),
          ),
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            color: listColor.activeButton,
            onPressed: (){
              Navigator.of(context).pop();
            },),
        ),
      body: FutureBuilder(
        future: ProfileService().fetchProfile(http.Client()),
        builder: (context, snapshot){
          if (snapshot.hasError) print(snapshot.error);
          return snapshot.hasData
              ? EditProfile(profile: snapshot.data,)
              : Center(child: CircularProgressIndicator());
        },
      ),
    );
  }
}

class EditProfile extends StatefulWidget {

   Profile profile;
   EditProfile({Key key ,this.profile}) : super(key : key);

  @override
  _EditProfile createState() => _EditProfile(profile: profile);
}

class _EditProfile extends State<EditProfile> {

  TextEditingController _nameController= new TextEditingController();
  TextEditingController _emailController= new TextEditingController();




  Profile profile ;
  _EditProfile({this.profile});

  @override
  Widget build(BuildContext context) {
    _nameController.text=this.profile.fullName;
    _emailController.text=this.profile.email;
    return Container(
      padding: EdgeInsets.fromLTRB(30, 0, 30, 0),
      constraints: BoxConstraints.expand(),
      width: MediaQuery.of(context).size.width,
      color: Colors.white,
      child: SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            SizedBox(
              height: 60,
            ),
            Image.asset('images/imagesperson.png'),
            Padding(
              padding: const EdgeInsets.fromLTRB(0, 50, 0, 20),
              child: StreamBuilder(
                //stream: authBloc.nameStream,
                  builder: (context, snapshot) =>
                      TextField(
                        controller: _nameController,
                        style: TextStyle(fontSize: 18, color: Colors.black),
                        decoration: InputDecoration(
                            errorText:
                            snapshot.hasError ? snapshot.error : null,
                            labelText: "Name",
                            prefixIcon: Container(
                                width: 50, child: Image.asset("images/ic_user.png")),
                            border: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Color(0xffCED0D2), width: 1),
                                borderRadius:
                                BorderRadius.all(Radius.circular(6)))),
                      )),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(0, 20, 0, 20),
              child: StreamBuilder(
                  builder: (context, snapshot) =>
                      TextField(
                        controller: _emailController,
                        style: TextStyle(fontSize: 18, color: Colors.black),
                        decoration: InputDecoration(
                            labelText: "Email",
                            errorText:
                            snapshot.hasError ? snapshot.error : null,
                            prefixIcon: Container(
                                width: 50, child: Image.asset("images/ic_mail.png")),
                            border: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Color(0xffCED0D2), width: 1),
                                borderRadius:
                                BorderRadius.all(Radius.circular(6)))),
                      )),
            ),
            Padding(
                padding: const EdgeInsets.fromLTRB(0, 20, 0, 10),
                child: Container(
                  //margin: EdgeInsets.fromLTRB(40,0, 0, 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      ButtonTheme(
                        height: 30,
                        child: RaisedButton(
                          // onPressed: _onSignUpClicked,
                          child: Text(
                            "Update",
                            style: TextStyle(color: Colors.white, fontSize: 18),
                          ),
                          onPressed: () async {
                            //Update an existing task
                            Map<String, dynamic> params = Map<String, dynamic>();
                            params["fullName"] = _nameController.text;
                            params["email"] = _emailController.text;
                            await ProfileService().updateAProfile(http.Client(), params);
                            Navigator.of(context).push(MaterialPageRoute( builder: (context)=>HomePage(pageIndex: 2,)));
                          },
                          color: listColor.activeButton,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.all(Radius.circular(6))),
                        ),
                      ),
                      Padding(padding: const EdgeInsets.fromLTRB(20, 0,0, 10)),
                      ButtonTheme(
                        height: 30,
                        child: RaisedButton(
                          // onPressed: _onSignUpClicked,
                          child: Text(
                            "Cancel",
                            style: TextStyle(color: Colors.white, fontSize: 18),
                          ),
                          onPressed: (){
                            Navigator.of(context).pop();
                          },
                          color: listColor.activeButton,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.all(Radius.circular(6))),
                        ),
                      ),
                    ],
                  ),
                )
            ),
          ],
        ),
      ),
    );
  }
}