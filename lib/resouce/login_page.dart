import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:toiecapp/config/app_config.dart';
import 'package:toiecapp/constants/constant.dart';
import 'package:toiecapp/model/validator.dart';
import 'package:toiecapp/resouce/home_page.dart';
import 'package:toiecapp/resouce/part_page.dart';
import 'package:toiecapp/resouce/register_page.dart';
import 'package:email_validator/email_validator.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class LoginPage extends StatefulWidget{

  _LoginPageState createState()=>_LoginPageState();

}

class _LoginPageState extends State<LoginPage>{
  SharedPreferences sharedPreferences;

  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passController = TextEditingController();

  var _userErr="";
  bool isValidEmail =false;
  bool isValidPassworf=false;
  bool is_loading=false;
  String _notice="";


  @override
  void initState() {
    super.initState();
    checkLoginStatus();

  }
  checkLoginStatus() async {
    sharedPreferences = await SharedPreferences.getInstance();
    print(sharedPreferences.getString("token")+"sdsdas");
  }
  void showTopShortToast(String notice) {
    Fluttertoast.showToast(
        msg: notice,
        textColor: Colors.black,
        backgroundColor: listColor.redAccent,
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.TOP,
        timeInSecForIosWeb: 2,
        fontSize:20
    );
  }

  SingIn(String username, password) async{
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    Map datas={
      'username': username,
      'password': password,
    };
    var jsonResponse = null;
    var response = await http.post(pod_urlconst.URL_LOGIN,body: datas);
    print("sda");
    if(response.statusCode == 200){
      print("232");
      jsonResponse= json.decode(response.body);
      if(jsonResponse != null){
        print(jsonResponse);
        setState(() {
          is_loading=false;
        });
        sharedPreferences.setString("token", jsonResponse['data']['accessToken']);
        print(sharedPreferences.getString("token"));
        Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (BuildContext context)=>  HomePage(pageIndex: 0,)) , (Route<dynamic> route) => false);
      }
    }else {
      setState(() {
        is_loading = false;
      });
      _notice="login fail";
      showTopShortToast(_notice);
     // SingIn(_emailController.text, _passController.text);
    }
  }
  String validateEmaill(String value) {
    if (value.isEmpty) {
      return 'Please enter email';
    }
  }
  _loginClick(){
    setState(() {
      if(validateEmaill(_emailController.text)!=null){
        isValidEmail=true;
      }else {
        isValidEmail=false;
      }
      if(validator().loginPassword(_passController.text)!=null){
        isValidPassworf=true;
      } else {
        isValidPassworf=false;
      }
      if(!isValidEmail && !isValidPassworf){
        setState(() {
          is_loading=true;
        });
        SingIn(_emailController.text, _passController.text);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.fromLTRB(30, 0, 30, 0),
        constraints: BoxConstraints.expand(),
        color: Colors.white,
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              SizedBox(
                height: 100,
              ),
              Image.asset('images/toiec.jpg'),
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 50, 0, 20),
                child: TextField(
                  controller: _emailController,
                  style: TextStyle(fontSize: 18, color: Colors.black),
                  onChanged: (text){
                  },
                  decoration: InputDecoration(
                      labelText: "Username",
                      errorText: isValidEmail ? validateEmaill(_emailController.text) : null ,
                      prefixIcon: Container(
                          width: 50, child: Image.asset("images/ic_user.png")),
                      border: OutlineInputBorder(
                          borderSide:
                          BorderSide(color: listColor.activeButton, width: 1),
                          borderRadius: BorderRadius.all(Radius.circular(6)))),
                ),
              ),
              TextField(
                controller: _passController,
                style: TextStyle(fontSize: 18, color: Colors.black),
                obscureText: true,
                decoration: InputDecoration(
                    labelText: "Password",
                    errorText: isValidPassworf ? validator().loginPassword(_passController.text) : null ,
                    prefixIcon: Container(
                        width: 50, child: Image.asset("images/ic_lock.png")),
                    border: OutlineInputBorder(
                        borderSide:
                        BorderSide(color: listColor.activeButton, width: 1),
                        borderRadius: BorderRadius.all(Radius.circular(6)))),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 30, 0, 40),
                child: SizedBox(
                  width: double.infinity,
                  height: 52,
                  child: RaisedButton(
                    //onPressed: ,
                    child: Text(
                      "Log In",
                      style: TextStyle(color: Colors.white, fontSize: 18),
                    ),
                    onPressed: (){
                      _loginClick();
                    },
                    color: listColor.activeButton,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(6))),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 0, 0, 30),
                child: RichText(
                  text: TextSpan(
                      text: "New user? ",
                      style: TextStyle(color: Color(0xff606470), fontSize: 16),
                      children: <TextSpan>[
                        TextSpan(
                            recognizer: TapGestureRecognizer()
                              ..onTap = () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => RegisterPage()));
                              },
                            text: "Sign up for a new account",
                            style: TextStyle(
                                color: listColor.activeButton, fontSize: 16))
                      ]),
                ),
              )
            ],
          ),
        ),
      ),
    );

  }

}


