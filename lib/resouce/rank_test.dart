import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:toiecapp/constants/constant.dart';
import 'package:toiecapp/constants/fakeData.dart';
import 'package:toiecapp/model/group_question.dart';
import 'package:toiecapp/model/rank.dart';
import 'package:toiecapp/model/tests.dart';
import 'package:toiecapp/service/Rate_rank_service.dart';
import 'package:toiecapp/service/test_service.dart';
import 'package:http/http.dart' as http;

class RankPage extends StatelessWidget{


  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.grey[200],
        appBar: AppBar(
          backgroundColor: listColor.activeButton,
          title: Center(
            child: Text(
              "Top Rank",
              style: TextStyle(
                  color: listColor.barColor
              ),
            ),
          )
        ),
        body: FutureBuilder(
          future: RateRankService().fetchRanking(http.Client()),
          builder: (context, snapshot) {
            if (snapshot.hasError) print(snapshot.error);
            return snapshot.hasData
                ? TestRank(ranklist: snapshot.data)
                : Center(child: CircularProgressIndicator());
          },
        )
    );
  }
}

class TestRank extends StatelessWidget {

  List<Rank> ranklist;

  TestRank({Key key,this.ranklist}) : super(key :key);

  Color Corlor(int index){
    if(index==0){
      return Colors.redAccent.withOpacity(0.5);
    }else if(index==1){
      return Colors.green.withOpacity(0.8);
    }else if(index==2){
      return Colors.blue.withOpacity(0.8);
    }else{
      return listColor.barColor;
    }
  }
  Color ColorText(int index){
    if(index==0|| index==1||index==2) {
      return Colors.white;
    }else{
      return listColor.activeButton;
    }
  }

  getNameAvatar(String name) {
    List<String> listCharacter =  name.split(' ').map((e) => e[0]).toList();
    return listCharacter[0]+listCharacter[listCharacter.length-1];
  }
    @override
    Widget build(BuildContext context) {
      return Scaffold(
        body: Container(
          child: ListView.separated(
            separatorBuilder: (_, __) => Divider(height: 1, color: Colors.green,),
            itemBuilder: (_, index){
              return Container(
                margin: EdgeInsets.fromLTRB(10, 5, 10, 5),
                padding: EdgeInsets.fromLTRB(20, 0, 10, 0),
                height: 100,
                  decoration: BoxDecoration(
                    boxShadow: [
                      BoxShadow(
                        blurRadius: 1,
                        spreadRadius: 0.2,
                        color: Colors.grey[200],
                      )
                    ],
                    borderRadius: BorderRadius.circular(20),
                    color: Corlor(index),
                  ),
                  child: Stack(
                      children: <Widget>[
                        Align(
                          alignment: Alignment.centerLeft,
                          child: Container(
                            width: 50,
                            height: 50,
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                border: Border.all(width: 1,color: listColor.activeButton),
                                color: Colors.white
                            ),
                            child: Center(
                              child: Text(getNameAvatar(this.ranklist[index].fullName)),
                            ),
                          ),
                        ),
                        Align(
                          alignment: Alignment.centerRight,
                          child: Container(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: <Widget>[
                                Text(this.ranklist[index].fullName,style:
                                TextStyle(
                                  color: ColorText(index),
                                  fontSize: 27.0,
                                ),),
                                Padding(padding: EdgeInsets.fromLTRB(10, 0, 0, 0),),
                                Text(this.ranklist[index].numberTest.toString(),style:
                                TextStyle(
                                  color: Colors.yellow,
                                  fontSize: 30.0,
                                ),),
                              ],
                            )
                          ),
                        ),
                      ]
                  )
              );
            },
            itemCount: this.ranklist.length,
          ),
        ),
      );
    }
}



