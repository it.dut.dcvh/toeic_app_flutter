import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:toiecapp/config/app_config.dart';
import 'package:toiecapp/constants/constant.dart';
import 'package:toiecapp/constants/fakeData.dart';
import 'package:toiecapp/resouce/group_question_item.dart';
import 'package:toiecapp/model/group_question.dart';
import 'package:toiecapp/resouce/stat_ratings.dart';
import 'package:toiecapp/service/Rate_rank_service.dart';
import 'package:toiecapp/service/group_question_service.dart';
import 'package:toiecapp/resouce/show_result_test.dart';
import 'package:http/http.dart' as http;
import 'package:toiecapp/service/history_service.dart';

class GroupQuestionPage extends StatefulWidget {

  final int testId;
  
  GroupQuestionPage({this.testId});

  @override
  State<StatefulWidget> createState () {
    return _GroupQuestionPage(testId);
  }

}

class _GroupQuestionPage extends State<GroupQuestionPage> {
  ScrollController _scrollController= new ScrollController();
  int current;
  GroupQuestion currentGroupQuestion;
  List<GroupQuestion> listGroupQuestions;
  List<Record> listRecords = new List<Record>();
  List<ChooseAnswer> listChooseAnswers = new List<ChooseAnswer>();
  int testId;
  int totalQuestionsCorrect = 0;
  int totalQuestions = 0;
  AudioPlayer audioPlayer = AudioPlayer();
  List<GroupValue> groupValues = new List<GroupValue>();

  double maxVol = 100;
  double volume = 50;
  List<IconData> speakerIcons = [Icons.volume_off, Icons.volume_mute, Icons.volume_down, Icons.volume_up];
  List<IconData> playIcons = [Icons.play_arrow, Icons.pause, Icons.replay];
  int indexIconVolume = 2;
  int indexIconPlay=1;

  _GroupQuestionPage(this.testId);




  @override
  void initState() {
    super.initState();
//    WidgetsBinding.instance.addObserver(this);
    GroupQuestionService().fetchGroupQuestions(http.Client(), testId).then((response) {
      this.listGroupQuestions = response;
      setState(() {
        current = 0;
        maxVol;
        volume;
        indexIconVolume;
        totalQuestions = getTotalQuestion();
        currentGroupQuestion = listGroupQuestions[current];
        String playUrl = pod_urlconst.URL_PUBLIC_AUDIO+currentGroupQuestion.audioPath;
       this.audioPlayer.dispose();
        setTestState();
        this.audioPlayer.play(playUrl, isLocal: false);
      });
    });
//     this block to test with fake data
//     this.listGroupQuestions = listGroupQuestion;
//     current = 0;
//     totalQuestions = getTotalQuestion();
//     currentGroupQuestion = listGroupQuestions[current];
//
//    setTestState();
  }
//  @override
//  void dispose(){
//    super.dispose();
//    setState(() {
//      this.audioPlayer.stop();
//      this.audioPlayer.dispose();
//    });
//    WidgetsBinding.instance.removeObserver(this);
//  }
//  @override
//  void didChangeAppLifecycleState(AppLifecycleState state) {
//    super.didChangeAppLifecycleState(state);
//    if(state == AppLifecycleState.paused) {
//      print('App is in Background mode');
//    } else if(state == AppLifecycleState.resumed) {
//    }
//  }

  @override
  Widget build(BuildContext context) {
    return new WillPopScope(
        child: Scaffold(
            appBar: AppBar(
              backgroundColor: listColor.activeButton,
              leading: IconButton(
                  icon: new Icon(Icons.arrow_back_ios, color: listColor.barColor),
                  onPressed: () {
                    _onBackPage(context);
                  }
              ),
            ),
            body: Stack(
              children: [
                Container(
                    width: MediaQuery.of(context).size.width,
                    child: Builder(builder: (context) {
                      return this.listGroupQuestions != null ? _buildGroupQuestionItem(this.currentGroupQuestion) : Center(child: CircularProgressIndicator());
                    })
                ),
                Align(
                    alignment: Alignment.bottomCenter,
                    child: Container(
                      margin: const EdgeInsets.all(6.0),
                      height: 60,
                      width: MediaQuery.of(context).size.width,
                      decoration: new BoxDecoration(
                        color: listColor.activeButton.withOpacity(0.8),
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child:  Stack(
                        fit: StackFit.expand,
                        children: [
                          Align(
                              alignment: Alignment.centerLeft,
                              child: Container(
                                width: 40,
                                child: IconButton(
                                  icon: Icon(Icons.arrow_back_ios),
                                  color: Colors.white,

                                  onPressed: () {
                                    _getPrevQuestion();
                                  },
                                ),
                              )
                          ),
                          Align(
                              alignment: Alignment.center,
                              child: Container(
                                width: 100,
                                height: 40,
                                child: RaisedButton(
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(18.0),
                                      side: BorderSide(color: Colors.white)
                                  ),
                                  child: Center(
                                    child: Text(
                                      'Submit',
                                      style: TextStyle(
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                  color:  listColor.activeButton.withOpacity(0.0),
                                  onPressed: () {
                                    _onSubmit(context);
                                  },
                                ),
                              )
                          ),
                          Align(
                              alignment: Alignment.centerRight,
                              child: Container(
                                width: 40,
                                child: IconButton(
                                  icon: Icon(Icons.arrow_forward_ios),
                                  color: Colors.white,

                                  onPressed: () {
                                    _getnextQuestion();
                                  },
                                ),
                              )
                          )

                        ],
                      ),
                    )
                ),
              ],
            )
        )
        ,
        onWillPop: () {
          _onBackPage(context);
        }
    );
  }

  Widget _buildGroupQuestionItem(groupQuestions) {
    return Container(
      child: ListView(
        controller: _scrollController,
        children: [
          Container(
            margin: const EdgeInsets.all(10),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10.0),
            ),
            child: Column(
              children: [
                _buildAudioPlayer(groupQuestions),
                _buildText(groupQuestions),
                _buildImage(groupQuestions),
              ],
            )
          ),
          _buildListQuestion(groupQuestions.questions),
          Container(
            height: 60,
            width: MediaQuery.of(context).size.width,
          )
        ],
      )
    );
  } 

  Widget _buildAudioPlayer(groupQuestion) {
    if (groupQuestion.audioPath is String && groupQuestion.audioPath != '' && groupQuestion.audioPath != 'undefined') {
      return Container (
          width: MediaQuery.of(context).size.width,
          margin: const EdgeInsets.fromLTRB(10, 10, 10, 0),
          height: 60,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8.0),
              color: listColor.barColor,
              boxShadow: [(
                  BoxShadow(
                      color: Colors.grey[400],
                      blurRadius: 10.0,
                      spreadRadius: 2.0
                  )
              ),]
          ),
          child: Stack(children: [
            Align(
              alignment: Alignment.centerRight,
              child: Container(
                alignment: Alignment.centerRight,
                width: MediaQuery.of(context).size.width - 150,
                child: Stack(
                  children: [
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Icon(speakerIcons[indexIconVolume],),
                    ),
                    Slider(
                        label: 'vol',
                        activeColor: listColor.activeButton,
                        inactiveColor: Colors.grey[300],
                        value: this.volume,
                        min: 0.0,
                        max: this.maxVol,
                        onChanged: (double value) {
                          this.audioPlayer.setVolume(value/100);
                          setState(() {
                            this.volume = value;
                            if(value < 80 && value > 20) {
                              indexIconVolume = 2;
                            }
                            if(value >= 80) {
                              indexIconVolume = 3;
                            }

                            if(value <= 20 && value > 0) {
                              indexIconVolume = 1;
                            }

                            if (value == 0 ) {
                              indexIconVolume = 0;
                            }
                          });
                        }
                    )
                  ],
                ),
              ),
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: IconButton(
                icon: Icon(playIcons[indexIconPlay],
                  color: listColor.activeButton,
                ),
                iconSize: 40.0,
                onPressed: () {
                  if(indexIconPlay==1){
                    this.audioPlayer.pause();
                    setState(() {
                      this.indexIconPlay = 0;
                    });

                  }else if(indexIconPlay==0){
                    this.audioPlayer.resume();
                    setState(() {
                      this.indexIconPlay = 1;
                    });
                  }else if(indexIconPlay==2){
                    this.audioPlayer.resume();
                    setState(() {
                      this.indexIconPlay = 1;
                    });

                  }

                },
              ),
            )
          ],)
      );
    }
    return Container(
      width: MediaQuery.of(context).size.width,
    );
  }

  Widget _buildImage(groupQuestion) {
    return Builder(builder: (context) {
      if (groupQuestion.imagePath is String &&
          groupQuestion.imagePath.toString() != 'undefined' && 
          groupQuestion.imagePath.toString() != '') {
        return Container(
          width: MediaQuery.of(context).size.width,
          child: Image.network(
            pod_urlconst.URL_PUBLIC_IMAGE+groupQuestion.imagePath,
            fit: BoxFit.cover,
          ),
        );
      }
      return Container(
        width: MediaQuery.of(context).size.width,
      );
    });
  }

  Widget _buildText(groupQuestion) {
    return Builder(builder: (context) {
      if (groupQuestion.text != null && groupQuestion.text is String && groupQuestion.text != '') {
        return Container(
          margin: const EdgeInsets.fromLTRB(0, 20, 0, 0),
          padding: const EdgeInsets.fromLTRB(10, 10, 10, 20),
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(2.0),
            color: listColor.barColor,
            boxShadow: [
              new BoxShadow(
                color: Colors.grey[400],
                offset: new Offset(0.0, 5.0),
                blurRadius: 10,
              )
            ],
          ),
          child: Text(
            groupQuestion.text,
            style: TextStyle(
              fontSize: 15,
              color: listColor.activeButton
            ),
            textAlign: TextAlign.left,
            textWidthBasis: TextWidthBasis.parent,
            
          ),
        );
      }
        return Container(
          width: MediaQuery.of(context).size.width,
          height: 20,
        );
    });
  }

  Widget _buildListQuestion(questions) {
    return Builder(builder: (context) {
      return Container(
        width: MediaQuery.of(context).size.width,
        child: Column(
          children: _buildQuestion(questions),
        )
      );
    });
  }

  List<Widget> _buildQuestion(questions) {
    return new List<Widget>.generate(
      questions.length,
      (index) {
        return Builder(builder: (context) {
          return Container(
            width: MediaQuery.of(context).size.width,
            margin: const EdgeInsets.fromLTRB(20, 10, 20, 10),
            decoration: BoxDecoration(
              border: Border(
                bottom: BorderSide(
                  color: listColor.activeButton,
                  width: 1.0,
                )
              ),
            ),
            child: Column(
              children: [
                Container(
                  padding: const EdgeInsets.fromLTRB(5, 5, 5, 10),
                  width: MediaQuery.of(context).size.width,
                  child: Text(
                    questions[index].text,
                    textAlign: TextAlign.left,
                    textWidthBasis: TextWidthBasis.parent,
                    style: TextStyle(
                      fontSize: 20,
                    ),
                  ),
                ),
                Column(
                  children: _buildListAnswers(questions[index].answers, questions[index].questionId),
                )
                
              ],
            ),
          );
        });
      }
    );
  }

  List<Widget> _buildListAnswers (answers, questionId) {
    // int choose;
    return List<Widget>.generate(
      answers.length,
      (index) {
        int count = index;
        String value = getRecordAnswer(answers[index].answerId, questionId);
        String groupValue = this.groupValues[getGroupValue(questionId)].flag;
        String text = answers[index].text != null ? answers[index].text : '';
        return Container(
          child: RadioListTile(
            title: Text(text),
            value: value,
            groupValue: groupValue,
            onChanged: ( count) {
              setState(() {
                  int indexTemp = getGroupValue(questionId);
                  String flag = getRecordAnswer(answers[index].answerId, questionId);
                  this.groupValues[indexTemp].flag = flag;
              });

              _onChooseAnswer(answers[index].answerId, questionId, answers[index].isRight);
            },
          ),
        );
      }
    );
  }

  _onShowScore(context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return Container(
          margin: EdgeInsets.all(20),
          width: 200,
          height: 300,
          color: Colors.red,
          child: Builder( 
            builder: (context) { 
              return StarFeedbacks(testId: this.testId, listChooseAnswer: this.listChooseAnswers, listGroupQuestion: this.listGroupQuestions, totalCorrectAnswers: this.totalQuestionsCorrect);
            }),
        );
      });
  }

  _onBackPage(context){
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text("Notification"),
          content: new Text("Do you want to exit the assignment ?"),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("Yes"),
              onPressed: () {
                setState(() {
                  this.audioPlayer.dispose();
                  this.audioPlayer.stop();
                });
                Navigator.of(context).pop();
                Navigator.of(context).pop();
              },
            ),
            new FlatButton(
              child: new Text("No"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );

  }

  _onSubmit(context) {
    if (this.listChooseAnswers.length < this.groupValues.length) {
      int numUnchoose = this.groupValues.length - this.listChooseAnswers.length;
      showDialog(
        context: context,
        builder: (BuildContext context) {
          // return object of type Dialog
          return AlertDialog(
            title: new Text("Notification"),
            content: new Text("the test must be complete, you have $numUnchoose questions isn't  choose"),
            actions: <Widget>[
              // usually buttons at the bottom of the dialog
              new FlatButton(
                child: new Text("Close"),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
              new FlatButton(
                child: new Text("Keep Submit"),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
    }
    else {
      this.audioPlayer.stop();
      this.audioPlayer.dispose();
      Map<String, dynamic> params = Map<String, dynamic>();
      params["testId"] = this.testId.toString();
      params["correctAnswer"]= (((this.totalQuestionsCorrect/this.listChooseAnswers.length)*100).toInt()).toString();
      print(params);
      HistoryService().fetchHistoryCreate(http.Client(), params);
      Navigator.of(context).push(MaterialPageRoute( builder: (context) =>
      new StarFeedbacks(testId: this.testId, listChooseAnswer: this.listChooseAnswers, listGroupQuestion: this.listGroupQuestions, totalCorrectAnswers: this.totalQuestionsCorrect)));
    }


  }

  _onChooseAnswer(int answerId, int questionId, bool isCorrect) {
    if (this.listChooseAnswers != null && this.listChooseAnswers.length == 0) {
      this.listChooseAnswers.add(new ChooseAnswer(questionId, answerId, isCorrect));
      if (isCorrect) {
        this.totalQuestionsCorrect += 1;
      }
    }
    else if (this.listChooseAnswers != null) {
      bool isExistQuestionInList = false;

      this.listChooseAnswers.forEach((element) {
        if (element.questionId == questionId) {

          if (!element.isCorrect && isCorrect) {
            this.totalQuestionsCorrect += 1;
          }

          if (element.isCorrect && !isCorrect) {
            this.totalQuestionsCorrect -= 1;
          }
          element.answerId = answerId;
          element.isCorrect = isCorrect;
          isExistQuestionInList = true;
        }
      });

      if (!isExistQuestionInList) {
        this.listChooseAnswers.add(new ChooseAnswer(questionId, answerId, isCorrect));
        if (isCorrect) {
          this.totalQuestionsCorrect += 1;
        }
      }
      
      print('tong cau dung');
      print(this.totalQuestionsCorrect);
    }
  }

  _getnextQuestion () {
    setState(() {
      if (current < listGroupQuestions.length -1) {
        _scrollController.animateTo(0.0, duration: const Duration(microseconds: 300), curve: Curves.easeOut);
        current++;
        currentGroupQuestion = listGroupQuestions[current];
        // print(currentGroupQuestion.imagePath);
        print(current);
         this.audioPlayer.dispose();
         String playUrl = pod_urlconst.URL_PUBLIC_AUDIO+currentGroupQuestion.audioPath;
         this.audioPlayer.play(playUrl, isLocal: false);
      }
      
    });
  }

  _getPrevQuestion () {
    setState(() {
      if (current > 0) {
        _scrollController.animateTo(0.0, duration: const Duration(microseconds: 300), curve: Curves.easeOut);
        current--;
        currentGroupQuestion = listGroupQuestions[current];
        // print(currentGroupQuestion.audioPath);
        print(current);
         this.audioPlayer.dispose();
         String playUrl = pod_urlconst.URL_PUBLIC_AUDIO+currentGroupQuestion.audioPath;
         this.audioPlayer.play(playUrl, isLocal: false);
      }
    });
  } 

  int getTotalQuestion () {
    int total = 0;
    this.listGroupQuestions.forEach( (groupQuestion) {
      total += groupQuestion.questions.length;
    });

    return total;
  }

  setTestState() {
    if (this.listGroupQuestions != null && this.listGroupQuestions.length > 0) {
      this.listGroupQuestions.forEach((groupQuestion) {
        if (groupQuestion != null && groupQuestion.questions.length > 0) {
          groupQuestion.questions.forEach((e) {
            this.groupValues.add(new GroupValue(e.questionId));

            if (e.answers != null && e.answers.length > 0 ){
              e.answers.forEach((a) {
                this.listRecords.add(new Record(a.answerId, false, a.isRight, a.questionId));
              });
            }
          });
        }
      });
    }
  }

  String getRecordAnswer(int answerId, int questionId) {
    String record ;
    if (this.listRecords != null) {
      this.listRecords.forEach((e) {
        String temp = '$answerId-$questionId';

        if (e.flag == temp) {
          record=  e.flag;
        }
      });
    } 

    return record;
  }

  int getGroupValue(questionId) {
    int index = 0;
    int result = 0;
    if (this.groupValues != null) {
      this.groupValues.forEach((e) {
        if (e.questionId == questionId) {
          result = index;
        }
        index ++;
      });
    }

    return result;
  }
}
