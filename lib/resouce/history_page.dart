import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:toiecapp/constants/constant.dart';
import 'package:toiecapp/model/histories.dart';
import 'package:toiecapp/service/history_service.dart';
import 'package:http/http.dart' as http;

class HistoryPage extends StatelessWidget{


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200],
        appBar: AppBar(
          backgroundColor: listColor.activeButton,
          title: Text(
            "History",
            style: TextStyle(
                color: listColor.barColor
            ),
          ),
          leading: IconButton(
            icon: Icon(Icons.arrow_back_ios),
            color: listColor.barColor,
            onPressed: (){
              Navigator.of(context).pop();
            },),
        ),
      body: FutureBuilder(
        future: HistoryService().fetchHistory(http.Client()),
        builder: (context, snapshot) {
          if (snapshot.hasError) print(snapshot.error);
          return snapshot.hasData
              ? HistoryList(listHistory : snapshot.data)
              : Center(child: CircularProgressIndicator());
        },
      )
    );
  }
}

class HistoryList extends StatelessWidget{

  List<History> listHistory;
  HistoryList({Key key,this.listHistory}) : super(key : key);

  @override
  Widget build(BuildContext context) {
    return  Container(
        child: ListView.builder(
          itemBuilder: (context,index){
            return Card(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10)
                ),
                child: Row(
                  children: <Widget>[
                    Padding(padding: const EdgeInsets.fromLTRB(0, 20,0,0)),
                    Expanded(
                      child: Container(
                        decoration: BoxDecoration(
                            boxShadow: [
                              new BoxShadow(
                                  color: Colors.grey[400],
                                  blurRadius: 10,
                                  spreadRadius: 0.2
                              )
                            ],
                            color:listColor.barColor,
                            borderRadius: BorderRadius.circular(10)
                        ),
                        child: Column(
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.fromLTRB(10,10, 10, 5),
                              width: MediaQuery.of(context).size.width,
                              child: Text(this.listHistory[index].testName,
                                style: TextStyle(
                                  fontSize: 20,
                                  color: listColor.activeButton,
                                ),),
                              decoration: BoxDecoration(
                                border: Border(
                                  bottom: BorderSide(
                                    width: 1,
                                    color: Colors.grey[200]
                                  )
                                )
                              ),
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width,
                              child:  Row(
                                children: <Widget>[
                                  Container(
                                    padding: EdgeInsets.fromLTRB(10,10, 0, 10),
                                    child: Text(this.listHistory[index].correctAnswer.toString(),style: TextStyle(
                                      fontSize: 24,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.grey,
                                    ),),
                                  ),
                                  Spacer(),
                                  Container(
                                    padding: EdgeInsets.fromLTRB(0,10, 10, 10),
                                    child: Text(this.listHistory[index].date.substring(0,19).replaceAll('T', ' '),style: TextStyle(
                                      fontSize: 16,
                                      color: Colors.grey[400],
                                    ),),
                                  ),
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                    )
                  ],
                )
            );
          },
          itemCount: listHistory.length,
        )
    );
  }
}
