import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:toiecapp/constants/constant.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:toiecapp/model/profile.dart';
import 'package:toiecapp/resouce/change_password_page.dart';
import 'package:toiecapp/resouce/edit_profile_page.dart';
import 'package:toiecapp/resouce/history_page.dart';
import 'package:toiecapp/resouce/login_page.dart';
import 'package:toiecapp/service/profile_service.dart';
import 'package:http/http.dart' as http;

class ProfilePage extends StatelessWidget{

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FutureBuilder(
        future: ProfileService().fetchProfile(http.Client()),
        builder: (context, snapshot) {
          if (snapshot.hasError) print(snapshot.error);
          return snapshot.hasData
              ? ProfileK(profile : snapshot.data)
              : Center(child: CircularProgressIndicator());
        },
      ),
    );
  }
}


class ProfileK extends StatelessWidget{

  Profile profile;
  ProfileK({Key key,this.profile}) : super(key : key);


  SharedPreferences sharedPreferences;

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        body: new Stack(
          children: <Widget>[
            ClipPath(
              child: Container(color: listColor.activeButton.withOpacity(0.8)),
              clipper: getClipper(),
            ),
            Positioned(
                width: MediaQuery.of(context).size.width,
                top: MediaQuery.of(context).size.height / 5,
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    Container(
                        width: MediaQuery.of(context).size.width/3,
                        height: MediaQuery.of(context).size.width/3,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.all(Radius.circular(75.0)),
                            border: Border.all(
                              width: 3,
                              color: listColor.activeButton
                            ),
                            boxShadow: [
                              BoxShadow(blurRadius: 7.0, color: Colors.black)
                            ],
                        ),
                      child: Center(
                        child: Text(getNameAvatar(this.profile.fullName),style: TextStyle(
                          color: listColor.activeButton,
                          fontSize: 40.0,
                          fontWeight: FontWeight.bold
                        ),),
                      ),
                        
                    ),
                    SizedBox(height: MediaQuery.of(context).size.height/35),
                    Text(profile.fullName,
                      style: TextStyle(
                          fontSize: 30.0,
                          color: Colors.grey,
                          fontWeight: FontWeight.bold,
                          fontFamily: 'Montserrat'),
                    ),
                    SizedBox(height: MediaQuery.of(context).size.height/35),
                    Text(profile.email,
                      style: TextStyle(
                          fontSize: 20.0,
                          color: Colors.grey,
                          fontStyle: FontStyle.italic,
                          fontFamily: 'Montserrat'),
                    ),
                    SizedBox(height: MediaQuery.of(context).size.height/15),
                    Container(
                        height: MediaQuery.of(context).size.height/23,
                        width: MediaQuery.of(context).size.width/2.3,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20),
                          color: listColor.barColor,
                          boxShadow: [
                            BoxShadow(
                                color: listColor.activeButton,
                                blurRadius: 1,
                                spreadRadius: 2
                            ),

                          ]
                      ),
                        child: FlatButton(
                          onPressed: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => EditProfilePage()));
                          },
                          child: Center(
                            child: Text(
                              'Edit Name',
                              style: TextStyle(color: listColor.activeButton,
                                fontFamily: 'Montserrat',),
                            ),
                          ),
                        ),),
                    SizedBox(height: MediaQuery.of(context).size.height/45),
                    Container(
                        height: MediaQuery.of(context).size.height/23,
                        width: MediaQuery.of(context).size.width/2.3,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20),
                          color: listColor.barColor,
                          boxShadow: [
                            BoxShadow(
                              color: listColor.activeButton,
                              blurRadius: 1,
                              spreadRadius: 2
                            ),
                          ]
                        ),
                        child: FlatButton(
                          onPressed: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => ChangePassword()));
                          },
                          child: Center(
                            child: Text(
                              'Change password',
                              style: TextStyle(color: listColor.activeButton, fontFamily: 'Montserrat'),
                            ),
                          ),
                        ),
                    ),
                    SizedBox(height: MediaQuery.of(context).size.height/45),
                    Container(
                        height: MediaQuery.of(context).size.height/23,
                        width: MediaQuery.of(context).size.width/2.3,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20),
                          color: listColor.barColor,
                          boxShadow: [
                            BoxShadow(
                                color: listColor.activeButton,
                                blurRadius: 1,
                                spreadRadius: 2
                            ),
                          ]
                      ),
                        child: FlatButton(
                          onPressed: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => HistoryPage()));
                          },
                          child: Center(
                            child: Text(
                              'History',
                              style: TextStyle(color: listColor.activeButton, fontFamily: 'Montserrat'),
                            ),
                          ),
                        ),),
                    SizedBox(height: MediaQuery.of(context).size.height/45),
                    Container(
                        height: MediaQuery.of(context).size.height/23,
                        width: MediaQuery.of(context).size.width/2.3,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20),
                          color: listColor.barColor,
                          boxShadow: [
                            BoxShadow(
                                color: listColor.activeButton,
                                blurRadius: 1,
                                spreadRadius: 2
                            ),
                          ]
                      ),
                        child:FlatButton(
                          onPressed: (){
                            _logout();
                            Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (BuildContext context) => LoginPage()), (Route<dynamic> route) => false);
                          },
                          child: Center(
                            child: Text(
                              'Log out',
                              style: TextStyle(color: Colors.redAccent, fontFamily: 'Montserrat'),
                            ),
                          ),
                        ),),
                  ],
                ))
          ],
        )
    );
  }

  _logout() async {
    sharedPreferences= await SharedPreferences.getInstance();
    this.sharedPreferences.clear();
  }
  
  getNameAvatar(String name) {
    List<String> listCharacter =  name.split(' ').map((e) => e[0]).toList();
    return listCharacter[0]+listCharacter[listCharacter.length-1];
  }
}
class getClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    var path = new Path();
    path.lineTo(0.0, size.height / 1.9);
    path.lineTo(size.width + 135, 0.0);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    // TODO: implement shouldReclip
    return true;
  }
}
