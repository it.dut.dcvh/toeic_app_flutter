import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:toiecapp/constants/constant.dart';
import 'package:toiecapp/model/group_question.dart';
import 'package:toiecapp/resouce/home_page.dart';
import 'package:toiecapp/resouce/show_result_test.dart';
import 'package:toiecapp/resouce/test_page.dart';
import 'package:toiecapp/service/Rate_rank_service.dart';
import 'package:http/http.dart' as http;

class StarFeedbacks extends StatefulWidget {

  final int testId;
  List<ChooseAnswer> listChooseAnswer;
  List<GroupQuestion> listGroupQuestion;
  int totalCorrectAnswers ;

  StarFeedbacks({this.testId, this.listChooseAnswer, this.listGroupQuestion, this.totalCorrectAnswers});


  @override
  _StarFeedbackState createState() => _StarFeedbackState(testId, listGroupQuestion, listChooseAnswer, totalCorrectAnswers);
}

class _StarFeedbackState extends State<StarFeedbacks> {
  List<GroupQuestion> listGroupQuestions;
  List<ChooseAnswer> listChooseAnswers = new List<ChooseAnswer>();
  int testId;
  int totalQuestionsCorrect;
  _StarFeedbackState(this.testId, this.listGroupQuestions, this.listChooseAnswers, this.totalQuestionsCorrect);

  var myFeedbackText = "COULD BE BETTER";
  var sliderValue = 0.0;

  IconData myFeedback1= FontAwesomeIcons.star,myFeedback2= FontAwesomeIcons.star,myFeedback3= FontAwesomeIcons.star,
      myFeedback4= FontAwesomeIcons.star,myFeedback5 = FontAwesomeIcons.star;
  Color myFeedbackColor1 = Colors.grey,myFeedbackColor2 = Colors.grey,myFeedbackColor3 = Colors.grey,
      myFeedbackColor4 = Colors.grey,myFeedbackColor5 = Colors.grey;

  void showTopShortToast() {
    Fluttertoast.showToast(
        msg: "Vui lòng đánh giá",
        textColor: Colors.black,
        backgroundColor: Colors.blue,
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.TOP,
        timeInSecForIosWeb: 4,
        fontSize:20
    );
  }
  Color getPointColor(int score){
    if(score >= 80){
      return Colors.green.withOpacity(0.8);
    }
    if(score<80 && score >=50){
      return Colors.blue.withOpacity(0.8);
    }
    if(score<50){
      return Colors.red.withOpacity(0.8);
    }
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Container(
          width: MediaQuery.of(context).size.width,
          color: Colors.white,
          child: Column(
            children: <Widget>[
              SizedBox(height:MediaQuery.of(context).size.height/40,),
              Container(
                padding: EdgeInsets.fromLTRB(0, 20, 0, 0),
                alignment: Alignment.center,
                  child: Container(
                    child: Center(
                      child: IconButton(
                        icon: Icon(Icons.close,size: 35.0,color:listColor.redAccent,),
                        onPressed: (){
                          Navigator.of(context).push(MaterialPageRoute(builder: (context)=>HomePage(pageIndex: 1,)));
                        },
                      ),
                    ),
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      border: Border.all(width: 1,color: listColor.redAccent),
                    ),
                  )
              ),
              SizedBox(height:MediaQuery.of(context).size.height/15,),
              Container(
                width: MediaQuery.of(context).size.width/2,
                height: MediaQuery.of(context).size.width/2,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  border: Border.all(width: 5,color: listColor.activeButton),
                ),
                child:Center(
                  child: Text((((this.totalQuestionsCorrect/listChooseAnswers.length)*100).toInt()).toString(),style: TextStyle(
                    fontSize: 60,
                    fontWeight: FontWeight.bold,
                    color: this.getPointColor(((this.totalQuestionsCorrect/listChooseAnswers.length)*100).toInt()),
                  ),),
                ),
              ),
              SizedBox(height: MediaQuery.of(context).size.height/23,),
              Container(
                child: Align(
                alignment: Alignment.bottomCenter,
                child: RaisedButton(
                  shape:RoundedRectangleBorder(borderRadius: new BorderRadius.circular(30.0)),
                  color: listColor.activeButton,
                  child: Text('Show result',
                    style: TextStyle(color: Color(0xffffffff)),),
                  onPressed: () {
                    Navigator.of(context)
                        .push(MaterialPageRoute(
                    builder: (_) => ResultPage(testId: this.testId, listChooseAnswer: this.listChooseAnswers, listGroupQuestion: this.listGroupQuestions, totalCorrectAnswers: this.totalQuestionsCorrect ))
                    );
                  }
                ),
              )),
              SizedBox(height: MediaQuery.of(context).size.height/8,),
              Container(
                margin: EdgeInsets.fromLTRB(10, 0, 10, 20),
                padding: EdgeInsets.fromLTRB(0, 20, 0, 0),
                child: Column(
                  children: <Widget>[
                    Text("Review",style: TextStyle(
                      color: listColor.activeButton,
                      fontSize: 20
                    ),),
                    Container(
                      padding: EdgeInsets.fromLTRB(10, 10, 0, 10),
                      child: Container(
                          width: MediaQuery.of(context).size.width,
                          child: Column(children: <Widget>[
                            Stack(
                              children: <Widget>[
                                Container(padding: EdgeInsets.only(left: 15),
                                  child: Container(
                                    child:Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                      children: StarWidget(),
                                    ),
                                  ),
                                ),
                                Container(
                                  padding: EdgeInsets.only(top: 2),
                                  child: Container(child: Slider(
                                    min: 0.0,
                                    max: 5.0,
                                    divisions: 5,
                                    value: sliderValue,
                                    activeColor: Colors.white.withOpacity(0),
                                    inactiveColor: Colors.white.withOpacity(0),
                                    onChanged: (newValue) {
                                      setState(() {
                                        sliderValue = newValue;
                                        if (sliderValue >= 1.0 ) {
                                          myFeedback1 = FontAwesomeIcons.solidStar;
                                          myFeedbackColor1 = Colors.yellow;
                                        }
                                        else if (sliderValue < 1.0 ){
                                          myFeedback1 = FontAwesomeIcons.star;
                                          myFeedbackColor1 = Colors.grey;

                                        }
                                        if (sliderValue >= 2.0 ) {
                                          myFeedback2 = FontAwesomeIcons.solidStar;
                                          myFeedbackColor2= Colors.yellow;
                                        }
                                        else if (sliderValue < 2.0 ){
                                          myFeedback2 = FontAwesomeIcons.star;
                                          myFeedbackColor2 = Colors.grey;

                                        }
                                        if (sliderValue >= 3.0 ) {
                                          myFeedback3 = FontAwesomeIcons.solidStar;
                                          myFeedbackColor3 = Colors.yellow;
                                        }
                                        else if (sliderValue < 3.0 ){
                                          myFeedback3 = FontAwesomeIcons.star;
                                          myFeedbackColor3 = Colors.grey;

                                        }
                                        if (sliderValue >= 4.0 ) {
                                          myFeedback4 = FontAwesomeIcons.solidStar;
                                          myFeedbackColor4 = Colors.yellow;
                                        }
                                        else if (sliderValue < 4.0 ){
                                          myFeedback4 = FontAwesomeIcons.star;
                                          myFeedbackColor4 = Colors.grey;

                                        }
                                        if (sliderValue >= 5.0 ) {
                                          myFeedback5 = FontAwesomeIcons.solidStar;
                                          myFeedbackColor5 = Colors.yellow;
                                        }
                                        else if (sliderValue < 5.0 ){
                                          myFeedback5 = FontAwesomeIcons.star;
                                          myFeedbackColor5 = Colors.grey;
                                        }
                                      });
                                    },
                                  ),),
                                ),
                              ],
                            ),
                            SizedBox(height:MediaQuery.of(context).size.height/20,),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Container(child: Align(
                                alignment: Alignment.bottomCenter,
                                child: RaisedButton(
                                  shape:RoundedRectangleBorder(borderRadius: new BorderRadius.circular(30.0)),
                                  color: listColor.activeButton,
                                  child: Text('Submit',
                                    style: TextStyle(color: Color(0xffffffff)),),
                                  onPressed: ()  async{
                                    print(sliderValue.toInt());
                                    int numstar=sliderValue.toInt();
                                    Map<String, dynamic> params = Map<String, dynamic>();
                                    params["rateStar"] = numstar.toString();
                                    params["testId"] = this.testId.toString();
                                    print(params);
                                    await RateRankService().fetchRate(http.Client(), params);
                                    Navigator.of(context).push(MaterialPageRoute(builder: (context) => HomePage(pageIndex: 1,)));
                                  },
                                ),
                              )),
                            ),
                            SizedBox(height:MediaQuery.of(context).size.height/30,),
                          ],)
                      ),
                    ),
                  ],
                ),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  border: Border.all(color: listColor.activeButton,width: 2),
                  boxShadow: [
                    new BoxShadow(
                      color: Colors.white,
                      blurRadius: 4,
                      spreadRadius: 0.2
                    )
                  ]
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  List<Widget> StarWidget(){
    List<Widget> myContainer = new List();
    myContainer.add(Container(child: Icon(
      myFeedback1, color: myFeedbackColor1, size: 35.0,)));
    myContainer.add(Container(child: Icon(
      myFeedback2, color: myFeedbackColor2, size: 35.0,)));
    myContainer.add(Container(child: Icon(
      myFeedback3, color: myFeedbackColor3, size: 35.0,)));
    myContainer.add(Container(child: Icon(
      myFeedback4, color: myFeedbackColor4, size: 35.0,)));
    myContainer.add(Container(child: Icon(
      myFeedback5, color: myFeedbackColor5, size: 35.0,)));
    return myContainer;
  }
  Widget _myAppBar() {
    return Container(
      height: 70.0,
      width: MediaQuery
          .of(context)
          .size
          .width,

      decoration: BoxDecoration(
        gradient: new LinearGradient(
          colors: [
            const Color(0xff662d8c),
            const Color(0xffed1e79),
          ],
          begin: Alignment.centerRight,
          end: new Alignment(-1.0, -1.0),
        ),
      ),
      child: Padding(
        padding: const EdgeInsets.only(top: 16.0),
        child: Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child:Container(child:IconButton(
                      icon: Icon(FontAwesomeIcons.arrowLeft,color: Colors.white,), onPressed: () {
                    //
                  }),),),
                Expanded(
                  flex: 5,
                  child:Container(child:Text('Rate', style:
                  TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.w600,
                      fontSize: 22.0
                  ),),),),
                Expanded(
                  flex: 1,
                  child:Container(child:IconButton(
                      icon: Icon(FontAwesomeIcons.star,color: Colors.white,), onPressed: () {
                    //
                  }),),),
              ],)
        ),
      ),
    );
  }
}

