import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:toiecapp/config/app_config.dart';
import 'package:toiecapp/model/parts.dart';
import 'package:http/http.dart' as http;

class PartService {

  Future<List<Part>> fetchParts(http.Client client) async {
    //How to make these URLs in a .dart file ?
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String token = sharedPreferences.getString('token');
    String authorization = 'Bearer ${token}';
    String url=pod_urlconst.URL_PART;
    print(url);
    final response = await client.get(url, headers: {'Authorization': authorization});
    if(response.statusCode == 200) {
      print(response.body);
      Map<String, dynamic> mapResponse = json.decode(response.body);
        final parts = mapResponse["data"].cast<Map<String, dynamic>>();
        final listOfPart = await parts.map<Part>((json) {
          return Part.fromJson(json);
        }).toList();
        return listOfPart;
      } else {
      throw Exception('Failed to load Todo from the Internet');
    }
  }
}

