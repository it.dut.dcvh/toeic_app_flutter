import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:toiecapp/config/app_config.dart';
import 'package:toiecapp/model/histories.dart';
import 'package:http/http.dart' as http;

class HistoryService {
  Future<List<History>> fetchHistory(http.Client client) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String token = sharedPreferences.getString('token');
    String authorization = 'Bearer ${token}';
    String url= pod_urlconst.URL_GET_HISTORY;
    print(url);
    final response = await client.get('$url',headers: {'Authorization': authorization});
    if (response.statusCode == 200) {
      print(response.body);
      Map<String, dynamic> mapResponse = json.decode(response.body);
      final historys = mapResponse["data"].cast<Map<String, dynamic>>();
      return historys.map<History>((json){
        return History.fromJson(json);
      }).toList();
    } else {
      print(response.body);
      throw Exception('Failed to load History');
    }
  }

  Future<History> fetchHistoryCreate(http.Client client, Map<String, dynamic> Params) async{
    SharedPreferences sharedPreferences= await SharedPreferences.getInstance();
    String token=sharedPreferences.getString("token");
    String authorization = 'Bearer ${token}';
    final response= await client.post(pod_urlconst.URL_ADD_HISTORY,headers: {'Authorization': authorization}, body: Params);
    if(response.statusCode==200){
      print(response.body);
    }else{
      print(response.body);
      throw Exception("fail");
    }

  }
}