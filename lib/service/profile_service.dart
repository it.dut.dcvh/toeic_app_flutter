import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:toiecapp/config/app_config.dart';
import 'package:toiecapp/constants/constant.dart';
import 'package:toiecapp/model/profile.dart';
import 'package:http/http.dart' as http;

class ProfileService {
  void showTopShortToast(String notice) {
    Fluttertoast.showToast(
        msg: notice,
        textColor: Colors.black,
        backgroundColor: listColor.redAccent,
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.TOP,
        timeInSecForIosWeb: 2,
        fontSize:20
    );
  }

  Future<Profile> fetchProfileById(http.Client client, int id) async {
    final String url = pod_urlconst.URL_GET_Proile+'$id';
    final response = await client.get(url);
    if (response.statusCode == 200) {
      Map<String, dynamic> mapResponse = json.decode(response.body);
        Map<String, dynamic> mapTask = mapResponse["data"];
        return Profile.fromJson(mapTask);
    } else {
      throw Exception('Failed to get detail task with Id = {id}');
    }
  }

  Future<Profile> fetchProfile(http.Client client) async {
    final String url = pod_urlconst.URL_GET_Proile;
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String token = sharedPreferences.getString('token');
    String authorization = 'Bearer ${token}';
    print(authorization);
    final response = await client.get(url,headers: {'Authorization': authorization} );
    if (response.statusCode == 200) {
      Map<String, dynamic> mapResponse = json.decode(response.body);
        return Profile.fromJson(mapResponse);
    } else {
      print(response.body);
      throw Exception('Failed to get profile');
    }
  }

  Future<Profile> updateAProfile(http.Client client,  Map<String, dynamic> params) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String token = sharedPreferences.getString('token');
    String authorization = 'Bearer ${token}';
    final response = await client.put(pod_urlconst.URL_EDIT_Proile,headers: {'Authorization': authorization}, body: params);
    if (response.statusCode == 200) {
      print(response.body);
     // final responseBody = await json.decode(response.body);
      //return Profile.fromJson(responseBody);
    } else {
      print(response.body);
      throw Exception('Failed to update a Task. Error: ${response.toString()}');
    }
  }

  Future<Profile> updatePassword(http.Client client,  Map<String, dynamic> params) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String token = sharedPreferences.getString('token');
    String authorization = 'Bearer ${token}';
    final response = await client.put(pod_urlconst.URL_CHAGNEPASSWORD,headers: {'Authorization': authorization}, body: params);
    if (response.statusCode == 200) {
      print(response.body);
    } else {
      final responseBody= json.decode(response.body);
      String s= responseBody["name"];
      showTopShortToast(s);
      throw Exception('Failed to update a Task. Error: ${response.toString()}');
    }
  }







}