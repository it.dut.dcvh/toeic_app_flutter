import 'dart:convert';
import 'package:flutter/foundation.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:toiecapp/config/app_config.dart';
import 'package:toiecapp/model/group_question.dart';
import 'package:http/http.dart' as http;

class GroupQuestionService {

  List<GroupQuestion> parseResults(String responseBody) {
    final parsed = json.decode(responseBody).cast<Map<String, dynamic>>();

    return parsed.map<GroupQuestion>((json) => GroupQuestion.fromJson(json)).toList();
  }

  Future<List<GroupQuestion>> fetchResults(http.Client client) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String token = sharedPreferences.getString('token');
    String authorization = 'Bearer ${token}';
    String testid= "?testId=";
    final response = await client.get('https://api.myjson.com/bins/j5xau');
    // Use the compute function to run parseResults in a separate isolate
    return compute(parseResults, response.body);
  }

  Future<List<GroupQuestion>> fetchGroupQuestions(http.Client client, int testId) async {
    //How to make these URLs in a .dart file ?
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String token = sharedPreferences.getString('token');
    String authorization = 'Bearer ${token}';
    String testid= "?testId=";
    String url= pod_urlconst.URL_GET_GROUPQUESTION;
    final response = await client.get('$url$testid$testId', headers: {'Authorization': authorization});
    if(response.statusCode == 200) {
      Map<String, dynamic> mapResponse = json.decode(response.body);
      final grouquestions = mapResponse["data"].cast<Map<String, dynamic>>();
      final listOfGroupQuestion = await grouquestions.map<GroupQuestion>((json) {
        return GroupQuestion.fromJson(json);
      }).toList();
      return listOfGroupQuestion;
    } else {
      throw Exception('Failed to load Todo from the Internet');
    }
  }

}