import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:toiecapp/config/app_config.dart';
import 'package:toiecapp/model/tests.dart';
import 'package:http/http.dart' as http;

class TestService {
  Future<List<Test>> fetchTest(http.Client client, int partID) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String token = sharedPreferences.getString('token');
    String authorization = 'Bearer ${token}';
    String partid= "?partId=";
    final response = await client.get(pod_urlconst.URL_TEST+'$partid$partID',headers: {'Authorization': authorization});
    if (response.statusCode == 200) {
      print(response.body);
      Map<String, dynamic> mapResponse = json.decode(response.body);
      final tests = mapResponse["list"].cast<Map<String, dynamic>>();
      return tests.map<Test>((json){
        return Test.fromJson(json);
      }).toList();
    } else {
      throw Exception('Failed to load Task');
    }
  }

  Future<List<Test>> fetchTestRank(http.Client client) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String token = sharedPreferences.getString('token');
    String authorization = 'Bearer ${token}';
    String partid= "?partId=";
    final response = await client.get(pod_urlconst.URL_TEST+'$partid',headers: {'Authorization': authorization});
    if (response.statusCode == 200) {
      print(response.body);
      Map<String, dynamic> mapResponse = json.decode(response.body);
      final tests = mapResponse["list"].cast<Map<String, dynamic>>();
      return tests.map<Test>((json){
        return Test.fromJson(json);
      }).toList();
    } else {
      throw Exception('Failed to load Task');
    }
  }
}