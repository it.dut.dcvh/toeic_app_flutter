import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:toiecapp/config/app_config.dart';
import 'package:http/http.dart' as http;
import 'package:toiecapp/constants/constant.dart';

class AccountService {

  void showTopShortToast(String notice) {
    Fluttertoast.showToast(
        msg: notice,
        textColor: Colors.black,
        backgroundColor: listColor.redAccent,
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.TOP,
        timeInSecForIosWeb: 2,
        fontSize:20
    );
  }

  void showTopShortToastSucces(String notice) {
    Fluttertoast.showToast(
        msg: notice,
        textColor: Colors.black,
        backgroundColor: listColor.activeButton,
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.TOP,
        timeInSecForIosWeb: 2,
        fontSize:20
    );
  }

  Future<Notice> singUp(http.Client client,String username, String fullname, String email, String password) async{
    Map datas={
      'username': username,
      'fullName': fullname,
      'email' : email,
      'password':password,
    };
    final response= await client.post(pod_urlconst.URL_SIGN_UP,body: datas);
    if(response.statusCode==200){
      final responseBody= json.decode(response.body);
      Notice notice = new Notice(isSuccess: responseBody["isSuccess"], message: responseBody["message"] );
      return notice;
    }else {
      final responseBody= json.decode(response.body);
      Notice notice = new Notice(isSuccess: responseBody["isSuccess"], message: responseBody["message"] );
      return notice;

//      String s= responseBody["message"];
//      showTopShortToast(s);
//      return responseBody["message"];

    }
  }
}

class Notice {
  bool isSuccess;
  String message;

  Notice({this.isSuccess, this.message});
}