class Rate {
  int testId;
  int numStar;
  String feedback;

  Rate({this.testId,this.numStar,this.feedback});

  factory Rate.fromJson(Map<String, dynamic> json){
    Rate newRate= Rate(
      testId: json["testId"],
      numStar: json["numStar"],
      feedback: json["feedback"]
    );
    return newRate;
  }
}