import 'package:toiecapp/model/questions.dart';

class GroupQuestion{
  int groupQuestionId;
  String text;
  String imagePath;
  String audioPath;
  List<Question> questions;
  int testId;

  GroupQuestion({this.groupQuestionId, this.text, this.imagePath, this.audioPath,
      this.questions, this.testId});

  factory GroupQuestion.fromJson(Map<String, dynamic> json) {

    List<Question> questions = json["questions"].map<Question>((questionJson) {
        return Question.fromJson(questionJson);
      }).toList();
    GroupQuestion newGroupQuestion = GroupQuestion(
      groupQuestionId: json['group_question_id'],
      text: json['text'],
      imagePath: json['image_path'],
      audioPath: json['audio_path'],
      testId: json['testId'],
      questions: questions,
    );
    return newGroupQuestion;
  }

}