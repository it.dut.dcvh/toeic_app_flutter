class Rank {
  int numberTest;
  String fullName;

  Rank({this.numberTest,this.fullName});

  factory Rank.fromJson(Map<String, dynamic> json){
    Rank newRate= Rank(
        numberTest: json["numberTest"],
        fullName: json["fullName"]
    );
    return newRate;
  }
}