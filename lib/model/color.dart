import 'package:flutter/material.dart';

class AppColor {
  Color barColor = Colors.white;
  Color borderColor = Colors.orange[400];
  Color outLineButtonTextColor = Colors.purple[400];
  Color grayBackGround = Colors.grey[200];
  Color activeButton = Colors.teal;
  Color inActiveButton = Colors.grey[400];
  Color mainTextColor = Colors.teal;
  Color normalTextColor = Colors.black;
  Color labelInActiveButtonColor = Colors.grey[400];
  Color lightOrange = Colors.orange[100];
  Color deepOrange =Colors.deepOrange[200];
  Color redAccent =Colors.redAccent[200];
  Color lightteal= Colors.teal[200];
}

