class Answer {

  int answerId;
  int questionId;
  String text;
  bool isRight;

  Answer({this.answerId, this.questionId, this.text, this.isRight});

  factory Answer.fromJson(Map<String, dynamic> json) {
    Answer answer = Answer(
      answerId: json['answerId'],
      text: json['text'],
      questionId: json["questionId"],
      isRight: json["isRight"]==1? true : false
    );
    return answer;
  }
}