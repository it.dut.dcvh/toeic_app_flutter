class Profile {
  int accountid;
  String fullName;
  String email;
  String password;
  String userName;

  Profile({this.accountid,this.password,this.email,this.fullName,this.userName});

  factory Profile.fromJson(Map<String, dynamic> json) {
    Profile newProfile = Profile(
      accountid: json['account_id'],
      fullName: json['full_name'],
      password: json['password'],
      email: json['email'],
      userName: json['username']
    );
    return newProfile;
  }
  factory Profile.fromProfile(Profile anotherprofile) {
    return Profile(
        accountid: anotherprofile.accountid,
        fullName: anotherprofile.password,
        email: anotherprofile.email,
        password: anotherprofile.password,
        userName : anotherprofile.userName,
    );
  }


}