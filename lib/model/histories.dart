class History {
  int testHistoryId;
  int accountId;
  int testId;
  String testName;
  int correctAnswer;
  String date;

  History({this.testHistoryId,this.accountId,this.testId,this.testName,this.correctAnswer,this.date});

  factory History.fromJson(Map<String, dynamic> json) {
    History newHistory = History(
        testHistoryId: json['testHistoryId'],
        accountId:  json['accountId'],
        testId: json['testId'],
        testName: json['testName'],
        correctAnswer: json['correctAnswer'],
        date: json['date']
    );
    return newHistory;
  }



}