class Account{
  int accountid;
  String username;
  String fullname;
  String email;
  String password;

  Account({this.accountid,this.username, this.fullname,this.email, this.password});

  factory Account.fromJson(Map<String, dynamic> json){
    Account newAccount= Account(
      accountid: json["accountid"],
      username: json["username"],
      fullname: json["fullname"],
      email: json["email"],
      password: json["password"],
    );
    return newAccount;
  }
}

