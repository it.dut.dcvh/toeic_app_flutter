class Record {
  int answerId;
  bool isChoose;
  bool isCorrect;
  int questionId;
  String flag = '';
  Record(this.answerId, this.isChoose, this.isCorrect, this.questionId) {
    this.flag = '$answerId-$questionId';
  }
}

class GroupValue {
  int questionId;
  String flag = '';
  GroupValue(this.questionId);
}

class ChooseAnswer {
  int questionId;
  int answerId;
  bool isCorrect;

  ChooseAnswer(this.questionId, this.answerId, this.isCorrect);
}