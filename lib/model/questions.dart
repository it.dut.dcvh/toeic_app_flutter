import 'package:toiecapp/model/answers.dart';

class Question {
  int questionId;
  int groupQuestionId;
  String text;
  List<Answer> answers;
  String explanation;

  Question({this.questionId,this.groupQuestionId,this.text,this.answers, this.explanation});
  
  factory Question.fromJson(Map<String, dynamic> json) {

    List<Answer> answers = json["answers"].map<Answer>((answerJson) {
        return Answer.fromJson(answerJson);
      }).toList();
    Question newGroupQuestion = Question(
      groupQuestionId: json['group_question_id'],
      questionId: json['questionId'],
      text: json['text'],
      answers: answers,
      explanation: json['explanation']
    );
    return newGroupQuestion;
  }

}