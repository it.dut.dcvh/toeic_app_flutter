import 'dart:math';

class Part {
  int partID;
  String partName;

  Part({this.partID,this.partName});

  factory Part.fromJson(Map<String, dynamic> json) {
    return Part(
        partID: json["partID"],
        partName: json["partName"]
    );
  }

}
