class Test {
  int partId;
  int testId;
  String name;
  int averagerate;
  Test({this.partId, this.testId, this.name,this.averagerate});

  factory Test.fromJson(Map<String, dynamic> json) {
    Test newTest = Test(
        partId: json['partId'],
        name: json['name'],
        testId: json['testId'],
        averagerate: json['averageRate']
    );
    return newTest;
  }
  factory Test.fromTask(Test anotherTask) {
    return Test(
        partId: anotherTask.partId,
        name: anotherTask.name,
        testId: anotherTask.testId,
        averagerate: anotherTask.averagerate
    );
  }
}

