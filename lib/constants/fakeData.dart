import 'package:toiecapp/model/answers.dart';
import 'package:toiecapp/model/group_question.dart';
import 'package:toiecapp/model/parts.dart';
import 'package:toiecapp/model/questions.dart';
import 'package:toiecapp/model/tests.dart';

//var partList=[
//  new Part(partID: 1,partName: "Part 1"),
//  new Part(partID: 2,partName: "Part 2"),
//  new Part(partID: 3,partName: "Part 3"),
//  new Part(partID: 4,partName: "Part 4"),
//  new Part(partID: 5,partName: "Part 5"),
//  new Part(partID: 6,partName: "Part 6"),
//  new Part(partID: 7,partName: "Part 7"),
//];

//var testList=[
//  new Test(partId: 1,testId: 1,name: "Test 1",),
//  new Test(partId: 1,testId: 2,name: "Test 2",),
//  new Test(partId: 1,testId: 3,name: "Test 3",),
//  new Test(partId: 1,testId: 4,name: "Test 4",),
//  new Test(partId: 1,testId: 5,name: "Test 5",),
//  new Test(partId: 1,testId: 6,name: "Test 6",),
//  new Test(partId: 1,testId: 7,name: "Test 7",),
//  new Test(partId: 1,testId: 8,name: "Test 8",),
//  new Test(partId: 1,testId: 9,name: "Test 9",),
//];

var listAnswer1 = [
  new Answer(questionId: 1, text: "A", answerId: 1, isRight: true),
  new Answer(questionId: 1, text: "A", answerId: 2, isRight: true),
  new Answer(questionId: 1, text: "A", answerId: 3, isRight: true),
  new Answer(questionId: 1, text: "A", answerId: 4, isRight: true),
];

var listAnswer2 = [
  new Answer(questionId: 2, text: "A", answerId: 5, isRight: true),
  new Answer(questionId: 2, text: "A", answerId: 6, isRight: true),
  new Answer(questionId: 2, text: "A", answerId: 7, isRight: true),
  new Answer(questionId: 2, text: "A", answerId: 8, isRight: true),
];

var listAnswer3 = [
  new Answer(questionId: 3, text: "A", answerId: 9, isRight: true),
  new Answer(questionId: 3, text: "A", answerId: 10, isRight: true),
  new Answer(questionId: 3, text: "A", answerId: 11, isRight: true),
  new Answer(questionId: 3, text: "A", answerId: 12, isRight: true),
];

var listAnswer4 = [
  new Answer(questionId: 4, text: "A", answerId: 13, isRight: true),
  new Answer(questionId: 4, text: "A", answerId: 21, isRight: true),
  new Answer(questionId: 4, text: "A", answerId: 32, isRight: true),
  new Answer(questionId: 4, text: "A", answerId: 41, isRight: true),
];

var question = [
  new Question(
      questionId: 1,
      groupQuestionId: 1,
      text: "ada aksdla akfja hieu asd  hieuaiei",
      answers: listAnswer1),
  new Question(
      questionId: 2,
      groupQuestionId: 1,
      text: "312 hieu hieu hieu31242342342343",
      answers: listAnswer2),
  new Question(
      questionId: 3,
      groupQuestionId: 2,
      text: "31231 ieas aha aha haha242342342343",
      answers: listAnswer3),
  new Question(
      questionId: 4,
      groupQuestionId: 3,
      text: "31231242342342343",
      answers: listAnswer4)
];

var listGroupQuestion = [
  new GroupQuestion(
      groupQuestionId: 1,
      text: 'The RaisedButton element needs to be used under the MaterialApp, so you will create the MaterialApp and then push the RaisedButton element to the MaterialApp’s home property.',
      audioPath: "2.mp3",
      testId: 1,
      imagePath: "anh1.jpg",
      questions: question),
  new GroupQuestion(
      groupQuestionId: 2,
      text: "cau hoi2",
      audioPath: "3.mp3",
      testId: 1,
      imagePath: "anh2.jpg",
      questions: question),
  new GroupQuestion(
      groupQuestionId: 3,
      text: "cau hoi3",
      audioPath: "4.mp3",
      testId: 1,
      imagePath: "undefined",
      questions: question),
];
